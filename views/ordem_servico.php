<?php
session_start();

require_once("../conn/conexao.php");

function getPagamento($id)
{
	global $conn;
	$sql = "SELECT tipo FROM pagamento WHERE id = $id";
	$res = mysqli_query($conn, $sql);
	while ($row = mysqli_fetch_array($res)) {
		$tipo = $row[0];
	}
	return $tipo;
}

if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}

// Trazendo informações da tabela OS
$sql = "SELECT id, id_orcamento, data_comp, data_vencimento, data_fechamento, obs, status,tipo,responsavel,valor_total
FROM ordem_serv";
$res = mysqli_query($conn, $sql);

$sqlVeiculo = "SELECT * FROM veiculo";
$resVeiculo = mysqli_query($conn, $sqlVeiculo);

$sqlFuncionario = "SELECT * FROM funcionario WHERE status = 1";
$resFuncionario = mysqli_query($conn, $sqlFuncionario);

$sql = "SELECT * FROM cliente WHERE status = 1";
$resCli = mysqli_query($conn, $sql);

$sql = "SELECT * FROM pagamento";
$resPagamento = mysqli_query($conn, $sql);
?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Ordens de Serviços
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddOS">Adicionar</button>
				<div style="margin-top: 10px">
					<label>Filtro</label>
					<select style="display: inline" class="form-control col-2" id="selectFiltro" onchange="filtro(this)">
						<option value="">Selecione um filtro</option>
						<option value="0">Não executado</option>
						<option value="1">Executado</option>
						<option value="2">Cancelado</option>
					</select>
				</div>
			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive" id="div-table">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Responsável</th>
							<th>Data de Competência</th>
							<th>Data de Vencimento</th>
							<th>Valor Total</th>
							<th>Tipo</th>
							<th>Observação</th>
							<th>Data de Fechamento</th>
							<th>Status</th>
							<th>Confirmação</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Responsável</th>
							<th>Data de Competência</th>
							<th>Data de Vencimento</th>
							<th>Valor Total</th>
							<th>Tipo</th>
							<th>Observação</th>
							<th>Data de Fechamento</th>
							<th>Status</th>
							<th>Confirmação</th>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {
							$status = $row['status'];

							if ($status == 0) {
								$status = "Não executado";
							}
							if ($status == 1) {
								$status = "Executado";
							}
							if ($status == 2) {
								$status = "Cancelado";
							}

							if (is_null($row['obs'])) {
								$obs = "Sem observação";
							} else {
								$obs = $row['obs'];
							}

							if (is_null($row['data_fechamento'])) {
								$data_fechamento = "Não houve";
							} else {
								$data_fechamento = date('d/m/y', strtotime($row['data_fechamento']));
							}
						?>
							<tr>
								<td><?= $row['responsavel']; ?></td>
								<td><?= date('d/m/Y', strtotime($row['data_comp'])); ?></td>
								<td><?= date('d/m/Y', strtotime($row['data_vencimento'])); ?></td>
								<td><?= $row['valor_total']; ?></td>
								<td><?= getPagamento($row['tipo']); ?></td>
								<td><?= $obs; ?></td>
								<td><?= $data_fechamento; ?></td>
								<td><?= $status; ?></td>
								<?php if ($row['status'] == 0) { ?>
									<td>
										<center>
											<a class="btn btn-danger btn-circle" href="php/cancela.ordem_serv.php?id=<?php echo $row['id']; ?>">
												<i class="fas fa-times"></i>
											</a>
											<a class="btn btn-success btn-circle" onclick="executadoOrdem(<?= $row['id']; ?>)">
												<i class="fas fa-check"></i>
											</a>
										</center>
									</td>
								<?php } else {
									echo "<td>$status</td>";
								} ?>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
<div class="modal fade" id="ExecutadoOrdemServico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Ordem de Serviço executada</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/aprova.ordem_serv.php" method="POST">
					<input id="id_ordem" name="id_ordem" type="hidden" />
					<div class="form-row">
						<div class="col">
							<textarea name="observacao" id="observacao" class="form-control" placeholder="Observação de OS"></textarea><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<label style="color:grey;">Hora Início</label>
							<input type="time" name="horaInicio" class="form-control" required><br>
						</div>
						<div class="col">
							<label style="color:grey;">Hora Fim</label>
							<input type="time" name="horaFim" class="form-control" required><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<select name="selectVeiculo" id="selectVeiculo" class="form-control" required>
								<option value="">Selecione o Veículo</option>
								<?php
								while ($row = mysqli_fetch_array($resVeiculo)) {
								?>
									<option value="<?= $row['id']; ?>"><?= $row['modelo'] . " / " . $row['placa']; ?></option>
								<?php
								}
								?>
							</select><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input type="number" step="0.1" name="kmInicio" class="form-control" placeholder="KM início" required><br>
						</div>
						<div class="col">
							<input type="number" step="0.1" name="kmFim" class="form-control" placeholder="KM fim" required><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<select name="selectFuncionario" id="selectFuncionario" class="form-control" required>
								<option value="">Selecione o(s) Funcionário(s)</option>
								<?php
								while ($row = mysqli_fetch_array($resFuncionario)) {
								?>
									<option value="<?= $row['id']; ?>"><?= $row['nome']; ?></option>
								<?php
								}
								?>
							</select><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input type="text" name="contratante" class="form-control" placeholder="Assinatura" required><br>
						</div>
					</div>
					<button class="btn btn-success" type="submit" style="float: right">Confirmar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddOS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastrar Ordem de Serviço</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/cadastra_ordem_serv.php" method="POST">
					<div class="form-row">
						<div class="col">
							<select name="responsavel_os" id="responsavel_os" class="form-control">
								<option value="">Selecione o Responsável</option>
								<?php while ($row = mysqli_fetch_array($resCli)) { ?>
									<option value="<?php echo $row['razao_social']; ?>"><?php echo $row['razao_social']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div><br>
					<div class="form-row">
						<div class="col">
							<select name="pagamento_os" id="pagamento_os" class="form-control">
								<option value="">Forma de Pagamento</option>
								<?php while ($row = mysqli_fetch_array($resPagamento)) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col">
							<input type="number" step="0.01" name="valor_os" id="valor_os" class="form-control" placeholder="Valor total">
						</div>
					</div><br>
					<div class="form-row">
						<div class="col">
							<label for="data_vencimento" style="color:grey;">Data de vencimento</label>
							<input type="date" name="data_vencimento" id="data_vencimento" class="form-control">
						</div>
						<div class="col">
							<label for="data_competencia" style="color:grey;">Data de competencia</label>
							<input type="date" name="data_competencia" id="data_competencia" class="form-control">
						</div>
					</div><br>
					<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	var quantidade_servico = 1;
	var quantidade_produto = 1;
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});


	function filtro(obj) {
		$.get('php/filtro_ordem_servico.php?aprovado=' + obj.value, function(data) {
			$('#div-table').html(data);
		});
	}

	function executadoOrdem(id_ordem) {
		$('#id_ordem').val(id_ordem);
		$('#ExecutadoOrdemServico').modal('show');

	}
</script>