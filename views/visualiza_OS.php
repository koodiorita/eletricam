<?php
error_reporting(0);
include_once("../conn/conexao.php");
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo'); 

$os_id = $_GET['id'];

$sql = "select 
            o.id,
            o.cliente,
            o.previsao_inicio,
            o.previsao_execucao,
            os.status,
            os.data_cad,
            os.data_comp,
            p.nome,
            v.modelo,
            v.placa,
            os.km_ini,
            os.km_fim,
            os.hora_ini,
            os.hora_fim,
            os.contratante,
            f.nome as nomeFunc

        from 
            orcamento as o 
            inner join pagamento as p on o.pagamento_id = p.id
            inner join ordem_serv as os on os.id_orcamento = o.id
            inner join veiculo as v on os.id_veiculo = v.id
            inner join funcionario as f on os.id_funcionario = f.id

        where 
            os.id = $os_id";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $orcamento_id         = $row['id'];
    $cliente    = $row['cliente'];
    $inicio     = $row['previsao_inicio'];
    $execucao   = $row['previsao_execucao'];
    $status     = $row['status'];
    $data_cad   = $row['data_cad'];
    $data_comp  = $row['data_comp'];
    $pagamento  = $row['nome'];
    $veiculo = $row['modelo']." / ".$row['placa'];
    $km_ini = $row['km_ini'];
    $km_fim = $row['km_fim'];
    $hora_ini = $row['hora_ini'];
    $hora_fim = $row['hora_fim'];
    $contratante = $row['contratante'];
    $funcionario = $row['nomeFunc'];


}

if($status == 0){
    $status = "Aguardando retorno";
}
if($status == 1){
    $status = "Orçamento Aprovado";
}
if($status == 2){
    $status = "Orçamento Reprovado";
}

$endereco = "";
$cnpj = "";



$sql = "select * from cliente where razao_social = '$cliente'";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $cnpj           = $row['cnpj'];
    $razao_social   = $row['razao_social'];
    $responsavel    = $row['responsavel'];
    $endereco       = $row['endereco'];
    $cidade         = $row['cidade'];
    $bairro         = $row['bairro'];
    $numero         = $row['numero'];
}

//TESTANDO SE TE MAIS DE 

?>



<!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Visualizar Ordem de Serviço</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">
  <link href="img/icon.png" rel="shortcut icon">
 
 <style>
 .header{
    margin: 15px;
 }
 label{
    display: block;
    margin-bottom: 0rem;
    font-size: 1.2rem;
 }
 .divider{
     position: relative;
     height: 1px;
     width: 100%;
     background: #101010;
     margin-top: 15px;
 }
 body{
     color: #000;
     margin: 5% 10% 0 10%;

 }
 .rodape{
    text-align: center;
    font-size: 10px;
 }

 </style>
</head>
<body >
    <div class="header" style="padding-left:-20px;" >
        <div class="form-row" style=" padding-left:10px;">
            <div class="col ml-4">
                <img src="../img/eletricam.png" width="70%" />
            </div>
            <div class="col text-right mr-4">
                <h5><b>Fones: (15) 3527.2207 | 98129.8287</b></h5>
                <h5><b>Rua Clementina dos Santos Theodoro, nº 381 - Vila Arruda - ITAPETININGA - SP</b></h5>
            </div>
        </div>
        <div class="form-row" style="padding-left:10px;">
            <div class="col">
                
            </div>
        </div>
    </div>
    <br>
    <label class="divider"></label>
    <br>
    <div class="descricao" style="padding-left:30px;">
   
        <div class="form-row">
            <div class="col">
                <label><b>Data: </b><span><?php echo date('d',strtotime($data_comp))." de ".strftime('%B',strtotime($data_comp))." de ".date('Y',strtotime($data_comp));?></span></label>
                <label><b>Nome: </b><span><?= $cliente;?></span></label>
                <label><b>CNPJ: </b><?= $cnpj?></label>
            </div>
            <div class="col">
                <label><b>Endereço: </b><span><?= $endereco;?></span></label>
                <label><b>Bairro: </b><?= $bairro?> <b>Município:</b> <?= $cidade?> <b>Estado:</b> SP</label>
            </div>
        </div>
    </div>
    <br>
    <label class="divider"></label>
    <br>
    <div class="servicos_produtos" style="padding-left: 30px">
    <div class="form-row">
            <div class="col-8">
                <label><b>Descrição</b></label>
            </div>
            <div class="col-2">
                <label><b>Qtd</b></label>
            </div>
            <div class="col-2">
                <label><b>Valor</b></label>
            </div>
        </div>
    <?php
        $sql = "select 
                    * 
                from 
                    orcamento as o 
                    inner join orcamento_serv as os on
                    o.id = os.orcamento_id
                    inner join servico as s on
                    os.servico_id = s.id 
                where 
                   o.id = $orcamento_id";
        $res = mysqli_query($conn, $sql);
        $qtd_serv = mysqli_num_rows($res);
        while($row = mysqli_fetch_array($res)){
            $valor_total_servico += $row['valor']*$row['qtd'];
    ?>
        
        <div class="form-row">
            <div class="col-8">
                <label><?php echo $row['nome'];?></label>
            </div>
            <div class="col-2">
                <label><?php echo $row['qtd'];?></label>
            </div>
            <div class="col-2">
                <label>R$ <?php echo number_format($row['valor']*$row['qtd'],2,',','.');?></label>
            </div>
        </div>
        <?php }?>
        <?php
         $sql = "select 
                    p.descricao,
                    op.qtd,
                    op.valor,
                    p.unidade 
                from 
                    orcamento as o 
                    inner join orcamento_prod as op on
                    o.id = op.orcamento_id
                    inner join produto as p on
                    op.produto_id = p.id 
                where 
                   o.id = $orcamento_id";
        $res = mysqli_query($conn, $sql);
        $qtd_prod = mysqli_num_rows($res);
        while($row = mysqli_fetch_array($res)){
            $valor_total_produto += $row['valor']*$row['qtd'];
    ?>
        
        <div class="form-row">
            <div class="col-8">
                <label><?php echo $row['descricao'];?></label>
            </div>
            <div class="col-2">
                <label><?php echo $row['qtd']." ".strtolower($row['unidade']);?></label>
            </div>
            <div class="col-2">
                <label>R$ <?php echo number_format($row['valor']*$row['qtd'],2,',','.');?></label>
            </div>
        </div>
        <?php }
        $total_geral = $valor_total_produto + $valor_total_servico;
        ?>
        <br>
        <div class="form-row">
            <div class="col-8">
                <label><b>Total Geral</b></label>
            </div>
            <div class="col-2">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label><b>R$ <?php echo number_format($total_geral,2,',','.');?></b></label>
            </div>
        </div>
        <br>
    </div>
    <br><br>
    <div class="rodape">
        <div class="form-row">
            <div class="col">
                
            </div>
            <div class="col" style="font-size: 20px !important;">
                <b>Hora:</b> <b>Início</b> <?= $hora_ini; ?>
            </div>
            <div class="col" style="font-size: 20px !important;">
                <b>Término</b> <?= $hora_fim; ?>
            </div>
            <div class="col">
                
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                
            </div>
            <div class="col" style="font-size: 20px !important;">
                <b>Km:</b> <b>Início</b> <?= $km_ini; ?>
            </div>
            <div class="col" style="font-size: 20px !important;">
                <b>Término</b> <?= $km_fim; ?>
            </div>
            <div class="col">
                
            </div>
        </div>
        <br>
        <label><b>Funcionários:</b> <?= $funcionario; ?></label><br>
        <label><b>Assinatura:</b> <?= $contratante; ?></label><br>

        <label><b>Fazendo hoje para sua tranquilidade amanhã.</b></label>
        
    </div>
</body>
<script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>
</html>