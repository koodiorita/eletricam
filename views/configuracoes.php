<?php
session_start();

require_once("../conn/conexao.php");

if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}

$sql = "SELECT valor FROM valor_hora";
$res_valor_hora = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_valor_hora)) {
	$valor_hora = $row[0];
}

?>
<div class="container-fluid">

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<form action="php/definir_valor_hora.php" method="POST" class="form-row">
				<div class="col-6">
					<label>Valor hora</label>
					<input type="number" value="<?= $valor_hora ?>" class="form-control col-10" name="valor_hora" id="valor_hora" placeholder="0">
					<br>
					<button type="submit" class="btn btn-success" style="float: left">Salvar</button>
				</div>
			</form>
		</div>
	</div>

</div>