<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}



$sql = "SELECT 
			o.id,
			o.cliente,
			o.status,
			o.data_cad
			FROM 
			orcamento as o 
			left join orcamento_serv as os ON
			o.id = os.orcamento_id
			left join orcamento_prod as op ON
			o.id = op.orcamento_id
		group by o.id
		";
$res = mysqli_query($conn, $sql);




?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Orçamentos

				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddOrc">Adicionar</button>
				<div style="margin-top: 10px">
					<label>Filtro</label>
					<select style="display: inline" class="form-control col-2" id="selectFiltro" onchange="filtro(this)">
						<option value="0">Aguardando Retorno</option>
						<option value="1">Aprovado</option>
						<option value="2">Reprovado</option>
					</select>
				</div>
			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive" id="div-table">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th width="5%">#</th>
							<th>Cliente</th>
							<th width="10%">Status</th>
							<th width="10%">Data de Inclusão</th>
							<th width="10%">Orçamento</th>
							<th width="11%">Emitir OS</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th width="5%">#</th>
							<th>Cliente</th>
							<th width="10%">Status</th>
							<th width="10%">Data de Inclusão</th>
							<th width="10%">Orçamento</th>
							<th width="11%">Emitir OS</th>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {
							$status = $row['status'];

							if ($status == 0) {
								$status = "Aguardando retorno";
							}
							if ($status == 1) {
								$status = "Pronto para executar";
							}
							if ($status == 2) {
								$status = "Reprovado";
							}

						?>
							<tr>
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['cliente']; ?></td>
								<td><?php echo $status; ?></td>
								<td><?php echo date('d/m/Y', strtotime($row['data_cad'])); ?></td>
								<td>
									<center>
										<a class="btn btn-primary btn-circle" target="_BLANK" href="views/print_orcamento.php?id=<?php echo $row['id']; ?>">
											<i class="fas fa-print"></i>
										</a>
									</center>
								</td>
								<?php if ($row['status'] == 0) { ?>
									<td>
										<center>
											<a class="btn btn-danger btn-circle" href="php/reprova_orcamento.php?id=<?php echo $row['id']; ?>">
												<i class="fas fa-times"></i>
											</a>
											<a class="btn btn-success btn-circle" onclick="aprovarOrcamento(<?php echo $row['id']; ?>)">
												<i class="fas fa-check"></i>
											</a>
										</center>
									</td>
								<?php } else if ($row['status'] == 1) { ?>
									<td>
										<center>
											<a class="btn btn-primary btn-circle" target="_BLANK" href="views/print_OS.php?id=<?php echo $row['id']; ?>">
												<i class="fas fa-print"></i>
											</a>
										</center>
									</td>
								<?php } else if ($row['status'] == 2) { ?>
									<td>Reprovado</td>
								<?php } ?>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>


<script>
	var quantidade_servico = 1;
	var quantidade_produto = 1;
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});


	function filtro(obj) {
		$.get('php/filtro_orcamento.php?aprovado=' + obj.value, function(data) {
			$('#div-table').html(data);
		});
	}
</script>

<div class="modal fade" id="AprovaOrcamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Aprovar Orçamento</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/aprova_orcamento.php" method="POST">
					<input id="id_orcamento" name="id_orcamento" type="hidden" />
					<div class="form-row">
						<div class="col">
							<label style="color: grey;">Data de competência:</label><br>
							<input type="date" name="dataCompetencia" id="dataCompetencia" class="form-control" required><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<label style="color: grey;">Data de vencimento:</label><br>
							<input type="date" name="dataVencimento" id="dataVencimento" class="form-control" required><br>
						</div>
					</div>
					<button class="btn btn-success" type="submit" style="float: right">Aprovar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	function aprovarOrcamento(id_orcamento) {
		$('#id_orcamento').val(id_orcamento);
		$('#AprovaOrcamento').modal('show');
	}
</script>