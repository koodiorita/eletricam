<?php
session_start();
// error_reporting(0);

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}

function getResponsavel($id, $tipo)
{
	global $conn;

	if ($tipo == "Fornecedor") {
		$sql = "SELECT nome FROM fornecedor WHERE id=$id";
	} else if ($tipo == "Funcionario") {
		$sql = "SELECT nome FROM funcionario WHERE id=$id";
	}
	$res = mysqli_query($conn, $sql);
	while ($row = mysqli_fetch_array($res)) {
		$responsavel = $row[0];
	}
	return $responsavel;
}

$sql  = "SELECT 
            c.id,
            c.id_fornecedor,
			c.tipo_responsavel,
            c.valor,
            c.vencimento,
            c.dataCompetencia,
			c.descricao,
			c.status
        FROM 
            `contas_pagar` as c
        where 
			c.status = 0 and
            month(c.vencimento) = month(now()) and year(c.vencimento) = year(now())
		";
$res = mysqli_query($conn, $sql);


$sql = "SELECT * FROM banco";
$resBanco = mysqli_query($conn, $sql);

?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}

	.show {
		display: block;
	}

	.hide {
		display: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3"><button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddPagarConta">Adicionar</button>
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Contas a Pagar</h4>
				</div>

				<div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
				<div class="col-2">
					<select class="form-control" name="banco_pesquisa" id="banco_pesquisa">
						<option value="">Selecione o Banco</option>
						<?php while ($row = mysqli_fetch_array($resBanco)) { ?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar_pagar()">Buscar</button></div>
			</div>

		</div>


		</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive" id="div-table">
			<table class="table table-bordered" id="dataTablePagar" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Vencimento</th>
						<th>Data de competência</th>
						<th>Valor</th>
						<th>Responsável</th>
						<th>Tipo de responsável</th>
						<th>Descricao</th>
						<th width="10%">Pagar</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$total = 0;
					while ($row = mysqli_fetch_array($res)) {
						$total += $row['valor'];
						if (is_null($row['dataCompetencia'])) {
							$data_competencia = "Sem data";
						} else {
							$data_competencia = date('d/m/Y', strtotime($row['dataCompetencia']));
						}
					?>
						<tr>
							<td><?php echo date('d/m/Y', strtotime($row['vencimento'])); ?></td>
							<td><?php echo $data_competencia ?></td>
							<td><?php echo "R$ " . number_format($row['valor'], 2, ',', '.'); ?></td>
							<td><?php echo getResponsavel($row['id_fornecedor'], $row['tipo_responsavel']); ?></td>
							<td><?php echo $row['tipo_responsavel'] ?></td>
							<td><?php echo $row['descricao']; ?></td>
							<?php if ($row['status'] == 0) { ?>
								<td>
									<center><button class="btn btn-danger btn-circle" onclick="pagar(<?php echo $row['id']; ?>)"><i class="fas fa-download"></i></button></center>
								</td>
							<?php } else { ?>
								<td>Pago</td>
							<?php } ?>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Vencimento</th>
						<th>Data de competência</th>
						<th><?php echo "R$ " . number_format($total, 2, ',', '.'); ?></th>
						<th>Responsável</th>
						<th>Tipo de responsável</th>
						<th>Descricao</th>
						<th width="10%">Pagar</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

</div>

<!-- PagarConta -->
<div class="modal fade" id="PagarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Pagar Conta</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/pagar_conta.php" method="POST" enctype="multipart/form-data">
					<input id="id_pagar" name="id_pagar" type="hidden" />
					<div class="form-row">
						<div class="col">
							<label style="color: grey;">Insira o comprovante</label><br>
							<input type="file" name="arquivo" id="arquivo" class="form-control"><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<label style="color: grey;">Selecione o banco</label>
							<select name="selectBanco" id="selectBanco" class="form-control">
								<?php
								$sql1 = "SELECT * FROM banco";
								$resultado = mysqli_query($conn, $sql1);
								if (mysqli_num_rows($resultado) > 0) {
									while ($row = mysqli_fetch_assoc($resultado)) {
										$idBanco = $row['id'];
										$nomeBanco = $row['nome'];
										echo
											"<option value='" . $idBanco . "'>" . $nomeBanco . "</option>";
									}
								} else {
									echo "<option value=''>	Nenhum banco cadastrado </option> ";
								}
								?>
							</select><br>
						</div>
					</div>
					<button class="btn btn-success" type="submit" style="float: right">Pagar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>



<!-- AddPagarConta -->
<div class="modal fade" id="AddPagarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Pagar Conta</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/cadastra_pagar_conta.php" method="POST" enctype="multipart/form-data">
					<div class="form-row" style="margin-bottom:10px">
						<input style="margin-right:3px;margin-left:6px;margin-top:3px;" type="radio" name="radio_for_func" id="radio_for_func" value="1" onclick="campos_form(this)" checked> Fornecedor |

						<input style="margin-right:3px;margin-left:3px;margin-top:3px;" type="radio" name="radio_for_func" id="radio_for_func" value="0" onclick="campos_form(this)"> Funcionário
					</div>
					<div id="div-radio-funcionario" class="hide">
						<select name="funcionario_pagar" id="funcionario_pagar" class="form-control">
							<option value="">Selecione um Funcionário</option>
							<?php
							$sql = "select * from funcionario WHERE status = 1";
							$res = mysqli_query($conn, $sql);
							while ($row = mysqli_fetch_array($res)) { ?>
								<option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
							<?php } ?>
						</select><br>
						<div class="form-row">
							<div class="col">
								<label style="color: grey;">Valor do pagamento:</label>
								<input type="number" step="0.01" placeholder="Valor" name="valor_pagar_func" id="valor_pagar_func" class="form-control">
							</div>
							<div class="col">
								<label style="color: grey;">Data de vencimento:</label>
								<input type="date" class="form-control" name="vencimento_pagar_func" id="vencimento_pagar_func"><br>
							</div>
						</div>
					</div>
					<div id="div-radio-fornecedor" class="show">
						<select name="fornecedor_pagar" id="fornecedor_pagar" class="form-control">
							<option value="">Selecione um Fornecedor</option>
							<?php
							$sql = "select * from fornecedor";
							$res = mysqli_query($conn, $sql);
							while ($row = mysqli_fetch_array($res)) { ?>
								<option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
							<?php } ?>
						</select><br>
						<div class="form-row">
							<div class="col-8"><input type="number" step="0.01" placeholder="Valor" name="valor_pagar" id="valor_pagar" class="form-control"><br></div>
							<div class="col-4"><input type="number" step="1" placeholder="Qtd Vezes" name="vezes_pagar" id="vezes_pagar" class="form-control"><br></div>
						</div>
						<label style="color: grey;">Data de competência:</label>
						<input type="date" class="form-control" name="dataCompetencia" id="dataCompetencia">
						<label style="color: grey;">Data de vencimento:</label>
						<input type="date" class="form-control" name="vencimento_pagar" id="vencimento_pagar"><br>
					</div>
					<textarea class="form-control" name="descricao" id="descricao" placeholder="Descricão"></textarea><br>
					<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		$('#dataTablePagar').DataTable({});
	});

	function pagar(id_pagar) {
		$('#id_pagar').val(id_pagar);
		$('#PagarConta').modal('show');
	}

	function buscar_pagar() {
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();
		var banco = $("#banco_pesquisa").val();

		if (data1.length > 0 || banco != "") {
			$.get("php/filtro_data_pagar.php?ini=" + data1 + "&fim=" + data2 + "&banco=" + banco, function(data) {
				$("#div-table").html(data);
			});
		} else {
			alert('Preencha pelo menos um dos campos.');
		}
	}

	function campos_form(obj) {
		if (obj.value == 1) {
			$('#div-radio-funcionario').removeClass('show');
			$('#div-radio-funcionario').addClass('hide');

			$('#div-radio-fornecedor').removeClass('hide');
			$('#div-radio-fornecedor').addClass('show');

			$('#funcionario_pagar').val("");
			$('#valor_pagar_func').val("");
			$('#vezes_pagar').val("");
			$('#descricao').val("");
		} else if (obj.value == 0) {
			$('#div-radio-fornecedor').removeClass('show');
			$('#div-radio-fornecedor').addClass('hide');

			$('#div-radio-funcionario').removeClass('hide');
			$('#div-radio-funcionario').addClass('show');


			$('#fornecedor_pagar').val("");
			$('#valor_pagar').val("");
			$('#vezes_pagar').val("");
			$('#dataCompetencia').val("");
			$('#vencimento_pagar').val("");
			$('#descricao').val("");
		}
	}
</script>