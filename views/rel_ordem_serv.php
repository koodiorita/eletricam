<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql  = "SELECT 
            c.id,
            f.nome,
            c.valor,
            c.vencimento,
            c.dataCompetencia,
			c.descricao,
			c.status
        FROM 
            `contas_pagar` as c
            inner join fornecedor as f ON
            c.id_fornecedor = f.id
        where 
			c.status = 0 and
            month(c.vencimento) = month(now()) and year(c.vencimento) = year(now())
		";
$res = mysqli_query($conn, $sql);




?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Relatório OS</h4>
				</div>

				<div class="col-3"><input type="date" id="filtro-data-pagar-1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-3"><input type="date" id="filtro-data-pagar-2" class="form-control" /></div>
				<div class="col-2">
					<select class="form-control" name="status_rel" id="status_rel">
						<option value="">Selecione o status</option>
						<option value="0">Não executado</option>
						<option value="1">Executado</option>
						<option value="2">Cancelado</option>
					</select>
				</div>
				<div class="col-2"><button style="float: right;margin-left: 10px; width:120px;" class=" btn btn-success" onclick="buscarRelatorio()">Buscar</button></div>
			</div>

		</div>


		</h4>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<div id="table-relatorio">

			</div>
		</div>
	</div>
</div>

</div>

<script>
	function buscarRelatorio() {

		var data1 = $("#filtro-data-pagar-1").val();
		var data2 = $("#filtro-data-pagar-2").val();
		var status = $("#status_rel").val();
		if (data1.length > 0 || status != "") {
			$.get("php/filtro_data_relatorio.php?ini=" + data1 + "&fim=" + data2 + "&status=" + status, function(data) {
				$("#table-relatorio").html(data);
			});
		} else {
			alert('Preencha pelo menos um dos campos.');
		}
	}
</script>