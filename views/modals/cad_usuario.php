	<!-- AddUser -->
    <div class="modal fade" id="AddUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Usuário</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_usuario.php" method="POST" enctype="multipart/form-data" >
                        <div class="form-row">
                            <div class="col">
                                <input name="nome" type="text" placeholder="Nome de Usuário" class="form-control"  /><br>
                            </div>
                            <div class="col">
                                <input name="avatar" type="file" class="form-control"  />
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col">
                                <input name="usuario" id="usuario" type="text" placeholder="Login" class="form-control" /><br>
                            </div>
                            <div class="col">
                                <input name="senha" id="senha" type="password" placeholder="Senha" class="form-control" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="email" placeholder="Email para contato" class="form-control" ><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label style="color:grey;">Permissões de Usuário</label>
                                <select multiple name="permissoes[]" id="permissoes">
                                  <option value="1">Painel de Controle</option>
                                  <option value="2">Orçamentos</option>
                                  <option value="3">Ordens de Serviços</option>
                                  <option value="4">Usuários</option>
                                  <option value="5">Clientes</option>
                                  <option value="6">Fornecedores</option>
                                  <option value="7">Funcionários</option>
                                  <option value="8">Veículos</option>
                                  <option value="9">Serviços</option>
                                  <option value="10">Produtos</option>
                                  <option value="11">Bancos</option>
                                  <option value="12">Contas a Receber</option>
                                  <option value="13">Contas a Pagar</option>
                                </select>
                            </div>
                        </div>
						<button class="btn btn-success" type="submit" style="float: right;">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				</div>
			</div>
		  </div>
          <script type="text/javascript">
              $(function() {
                $('#permissoes').selectize({
                    plugins: ['remove_button'],
                    delimiter: ',',
                    persist: true,
                    create: false
                });
                }); 

          </script>
          