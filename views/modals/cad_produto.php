<div class="modal fade" id="AddProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro Produto</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
            <div class="modal-body">
                <form action="php/cadastra_produto.php" method="POST"  >
                    <input type="hidden" id="id_produto" name="id_produto" >
                    <div class="form-row">
                        <div class="col">
                            <input name="descricao" id="descricao" type="text" placeholder="Nome do produto" class="form-control" required /><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <input name="custo" id="custo" type="number" step="0.01" placeholder="Custo"  class="form-control"  /><br>
                        </div>
                        <div class="col">
                            <input name="preco" id="preco" type="number" step="0.01" placeholder="Preço" class="form-control"  /><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <select class="form-control" name="unidade" >
                                <option value="UN">UNIDADE</option>
                                <option value="MT">METRO</option>
                            </select>
                        </div>
                        <div class="col">
                            <input name="estoque" id="estoque" type="number" step="0.01" placeholder="Estoque" class="form-control"  /><br>
                        </div>
                        <div class="col">
                            <input name="minimo" id="minimo" type="number" step="0.01" placeholder="Estoque mínimo"  class="form-control"  /><br>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col">
                            <textarea name="obs" id="obs" type="text" placeholder="Observação" class="form-control"  ></textarea><br>
                        </div>
                    </div>
                    <button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>
