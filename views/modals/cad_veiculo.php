	<!-- AddProd -->
    <div class="modal fade" id="AddVeic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Veículo</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_veiculo.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input name="modelo_veic" id="modelo_veic" type="text" placeholder="Modelo" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="marca_veic" id="marca_veic" type="text" placeholder="Marca" class="form-control"  required/><br>
                            </div>
                            <div class="col">
                                <input name="cor_veic" id="cor_veic" type="text" placeholder="Cor" class="form-control" required /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input name="ano_veic" id="ano_veic" type="number" placeholder="Ano" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="placa_veic" id="placa_veic" type="text" placeholder="Placa" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="renavam_veic" id="renavam_veic" type="text" placeholder="Renavam" class="form-control" required /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label style="color:grey;">Vencimento IPVA</label>
                                <input name="vencimento_veic" id="vencimento_veic" type="date" class="form-control" required /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input name="km_veic" id="km_veic" type="text" placeholder="Quilometragem" class="form-control"><br>
                            </div>
                        </div>

                        <button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                        <br>
					</form>
                </div>
				</div>
			</div>
		  </div>