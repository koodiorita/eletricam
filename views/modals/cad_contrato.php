<?php
    $sql = "select * from cliente where status = 1";
    $resCliente = mysqli_query($conn,$sql);

    $sql = "select * from servico where contratacao = 1";
    $resServico = mysqli_query($conn,$sql);

    $sql = "select * from pagamento";
    $resPagamento = mysqli_query($conn,$sql);

?>
    <div class="modal fade" id="AddContrato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Novo Contrato</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_contrato.php" method="POST" enctype="multipart/form-data" >
                        <input type="hidden" id="id_orcamento_con" name="id_orcamento_con" />
                        <div class="form-row">
                            <div class="col" >
                            Cliente
                            </div>
                            <div class="col" >
                            Serviço
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <select class="form-control" name="cliente_con" >
                                    <option id="id_cliente_con" value="">Selecione um Cliente</option>
                                    <?php 
                                        while($row = mysqli_fetch_array($resCliente)){ ?>
                                            <option value="<?php echo $row['id'];?>"><?php echo$row['razao_social'];?></option>
                                        <?php }?>
                                </select></br>
                            </div>
                            <div class="col">
                                <select class="form-control" name="servico_con">
                                    <option id="id_servico_con" value="">Selecione um Serviço</option>
                                    <?php 
                                        while($row = mysqli_fetch_array($resServico)){ ?>
                                            <option  value="<?php echo $row['id'];?>"><?php echo$row['nome'];?></option>
                                        <?php }?>
                                </select></br>
                            </div>
                        </div>
                        <hr><br>
                        <h3>Pagamento Implementação</h3>

                        <div class="form-row">
                            <div class="col-6">
                                Forma de Pagamento
                            </div>
                      
                         
                        </div>
                        
                        <div class="form-row">
                            <div class="col-6">
                                <select name="pagamento_imp_con" id="pagamento" class="form-control" data-teste="teste">
                                    <option value="">Selecione um Pagamento</option>
                                <?php while($row = mysqli_fetch_array($resPagamento)){?>
                                    <option data-juros="<?php echo $row['juros'];?>" data-vezes="<?php echo $row['qtd_vezes'];?>" value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                <?php }?>    
                                </select>    
                            </div>
                           
                        </div><br>

                        <div class="form-row">
                            <div class="col">Data Pagamento</div>
                            <div class="col">Desconto</div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                 <input id="pagamento_con" name="pagamento_con" type="date" class="form-control" readonly />
                            </div>
                            <div class="col">
                                <input class="form-control" id ="desconto_pagamento_con" name="desconto_pagamento_con" type="number" step="0.1" placeholder="Ex: 100" readonly/>
                            </div>
                        </div>
                        
                        <br>
                        <hr>
                        <h3>Pagamento Mensal</h3>

                        <div class="form-row">
                            <div class="col" >
                            Meses contratados
                            </div>
                            <div class="col" >
                            Data Primeiro Pagamento
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col" >
                                <input name="validade_con" type="number" class="form-control" placeholder="Ex: 12" required/>
                            </div>
                            <div class="col" >
                                <input name="primeiro_pagamento_con" type="date" class="form-control" required/>
                            </div>
                        </div>
                        <br>
                        <div class="form-row">
                            <div class="col">Desconto</div>
                        </div>

                        <div class="form-row">
                            <div class="col" >
                                <input class="form-control" name="desconto_con" type="number" step="0.1" placeholder="Ex: 100" />
                            </div>
                        </div>

                        <br>
                        <div class="form-row" style="margin-bottom: 7px">
                            <div class="col" style="align-self: center;">
                                <span style="color: #777; font-weight: bold">Esse contrato terá multa ? |</span>
                                <label for="Sim">Sim</label>
                                <input id="Sim" name="multa_con" type="radio" value="1" checked/>
                                <label for="Nao">Não</label>
                                <input id="Nao" name="multa_con" type="radio" value="0" />
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="col">Descrição</div>
                        </div>
                        <textarea class="form-control" name="descricao_con" placeholder=". . ." ></textarea></br>
                        
                        <div class="form-row">
                            <div class="col" >
                            Upload do contrato: 'PDF'
                            </div>
                        </div>

                        <input type="file" name="arquivo_con" class="form-control"  required/><br>
						<button class="btn btn-success" type="submit" style="float: right">Gerar Contrato</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>

          <script>
                
                $( "#pagamento" ).change(function() {
                    //alert($(this).val());
                    var juros = $(this).find(':selected').attr('data-juros');
                    var vezes = $(this).find(':selected').attr('data-vezes');
                    
                    if($(this).val() != 1){
                        
                        $("#juros").val(juros);
                        $("#juros").css('display','block');
                        $("#vezes").val(vezes);
                        $("#vezes").css('display','block');
                        $("#pagamento_con").attr("readonly", false); 
                        $("#desconto_pagamento_con").attr("readonly", false); 
                    }else{
                        $("#pagamento_con").attr("readonly", true); 
                        $("#desconto_pagamento_con").attr("readonly", true); 
                        $("#juros").val(juros);
                        $("#vezes").val(vezes);
                    }

                    
                });
          </script>