	<!-- AddBanco -->
    <div class="modal fade" id="AddBanco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro Banco</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_banco.php" method="POST"  >
                        <div class="form-row">
                            <div class="col-12">
                                <input id="nome_banco" name="nome_banco" type="text" placeholder="Nome do Banco" class="form-control" required /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <input id="agencia" name="agencia" type="text" placeholder="Ex: 0000-X" class="form-control"  /><br>
                            </div>
                            <div class="col">
                                <input id="conta" name="conta" type="text" placeholder="Ex: 00000000-0" class="form-control"  /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input id="endereco_banco" name="endereco_banco" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="cidade_banco" name="cidade_banco" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input id="valor_banco" name="valor_banco" type="text" placeholder="Valor em Banco" class="form-control" required />
                            </div>
                        </div><br>
                                
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>