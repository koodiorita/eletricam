	<!-- AddProd -->
    <div class="modal fade" id="AddCli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Cliente</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_cliente.php" method="POST" id="form-cli" >
                        <input type="hidden" id="id_orcamento_cli" name="id_orcamento_cli">
                        <div class="form-row">
                            <div class="col">
                                <input name="cnpj" type="text" placeholder="CNPJ / CPF" class="form-control" required /><br>
                            </div>
						    <div class="col">
                                <input name="ie" type="text" placeholder="Incrição Estadual" class="form-control" /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col">
                                <input name="razao" id="razao" type="text" placeholder="Razão Social / Nome Completo" class="form-control" required/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input name="email" type="text" placeholder="Email" class="form-control" /><br>
                            </div>
                            <div class="col">
                                <input name="responsavel" id="responsavel" type="text" placeholder="Responsavel" class="form-control"/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep" name="cep" type="text" placeholder="CEP" class="form-control" required/><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCep" type="button" placeholder="Cep" class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco" name="endereco" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input name="numero" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro" name="bairro" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade" name="cidade" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone" type="text" placeholder="Telefone" class="form-control" required/>
                            </div>
                            <div class="col">
                                <input name="telefone2" type="text" placeholder="Telefone 2 (Opcional)" class="form-control" />
                            </div>
                            <div class="col">
                                <input name="whatsapp" type="text" placeholder="WhatsApp" class="form-control" /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="dominio" type="text" placeholder="Ex: www.evolutionsoft.com.br" class="form-control" />
                            </div>
                        </div></br>         
                        <textarea name="observacao" type="text" placeholder="Digite uma observação" class="form-control" ></textarea><br>
						
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
        <script>
        $(document).ready(function () {
            $("#buscaCep").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#endereco").css("background","#eee");
                            $("#bairro").val(dados.bairro);
                            $("#bairro").css("background","#eee");
                            $("#cidade").val(dados.localidade);
                            $("#cidade").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });



       
        </script>