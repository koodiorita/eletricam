	 <?php
      $sql = "select * from servico";
      $res = mysqli_query($conn,$sql);

      $sql = "select * from produto";
      $resProduto = mysqli_query($conn,$sql);


      $sql = "select * from pagamento";
      $resPagamento = mysqli_query($conn,$sql);
      
      
      ?>
         <!-- AddOrcamento -->
         <div class="modal fade" id="AddOrc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Novo Orçamento</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<form action="php/cadastra_orcamento.php" method="POST" enctype="multipart/form-data" >
                            <div class="form-row">
                                <div class="col">
                                    <input id="cliente_orc" name="cliente_orc" type="text" class="form-control" placeholder="Nome do cliente"/></br>
                                </div>
                            </div>
                            <input type="hidden" id="qtd_servico" name="qtd_servico" value="1" />
                           <div id="div-servicos">
                           <div class="form-row">
                                <div class="col-6">
                                    <select  name="servico_orc_0" class="form-control">
                                        <option >Selecione um Serviço</option>
                                        <?php 
                                        while($row = mysqli_fetch_array($res)){ ?> 
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                        <?php }?>
                                    </select></br>
                                </div>
                                <div class="col-2">
                                    <input name="qtd_serv_0" type="number" step="0.01" class="form-control" placeholder="Qtd" />
                                </div>
                                <div class="col-2">
                                    <input name="valor_orc_0" type="number" step="0.01" class="form-control" placeholder="Valor" />
                                </div>
                                <div class="col-2">
                                    <button type="button" class="btn btn-success" onclick="novo_servico()" ><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                           </div>
                           <input type="hidden" id="qtd_produto" name="qtd_produto" value="1" />
                            <div id="div-produtos">
                                <div class="form-row">
                                    <div class="col-8">
                                        <select  name="produto_orc_0" class="form-control">
                                            <option >Selecione um Produto</option>
                                            <?php 
                                            while($row = mysqli_fetch_array($resProduto)){ ?> 
                                                <option value="<?php echo $row['id'];?>"><?php echo $row['descricao'];?></option>
                                            <?php }?>
                                        </select></br>
                                    </div>
                                    <div class="col-2">
                                        <input name="qtd_prod_0" type="number" step="0.01" class="form-control" placeholder="Qtd" />
                                    </div>
                                    <div class="col-2">
                                    <button class="btn btn-success" type="button" onclick="novo_produto()" ><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col">
                                    <input id="previsao_inicio_orc" name="previsao_inicio_orc" type="number" class="form-control" placeholder="Previsão de Inicio"/></br>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <input id="previsao_execucao_orc" name="previsao_execucao_orc" type="number" class="form-control" placeholder="Previsão de Execução"/></br>
                                </div>
                            </div>
                            <div class="form-row">
                            <div class="col">
                                <select name="pagamento_orc" id="pagamento_orc" class="form-control" >
                                    <option value="">Selecione um Pagamento</option>
                                <?php while($row = mysqli_fetch_array($resPagamento)){?>
                                    <option value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                <?php }?>    
                                </select>    
                            </div>
                           
                        </div><br>
							<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
							<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
						</form>
					</div>
				  </div>
			</div>
		  </div>

          <script>
         
            $(function() {
                $.get( "php/get_clientes.php", function( data ) {
                    var clientes = JSON.parse(data);
                
                    $("#cliente_orc" ).autocomplete({
                        source: clientes
                    });
                });
            });

            function novo_servico(){
               var div_row = document.createElement("div");
               div_row.setAttribute('class','form-row');
               div_row.setAttribute('id','div-servico-'+quantidade_servico);
               
               
               $.get( "php/get_servico_orc.php?id="+quantidade_servico, function( data ) {
                    div_row.innerHTML = data;
                    quantidade_servico++;
                    document.getElementById('qtd_servico').value = quantidade_servico;
               });


               document.getElementById('div-servicos').appendChild(div_row);
               
               
            }

            function remove_servico(id){
                var div = document.getElementById('div-servico-'+id);
                div.parentNode.removeChild(div);
                quantidade_servico--;
                document.getElementById('qtd_servico').value = quantidade_produto;
            }
            function novo_produto(){
               var div_row = document.createElement("div");
               div_row.setAttribute('class','form-row');
               div_row.setAttribute('id','div-produto-'+quantidade_produto);
               
               $.get( "php/get_produto_orc.php?id="+quantidade_produto, function( data ) {
                    div_row.innerHTML = data;
                    quantidade_produto++;
                    document.getElementById('qtd_produto').value = quantidade_produto;
                    
               });
               
               document.getElementById('div-produtos').appendChild(div_row);
               

            }
            function remove_produto(id){
                var div = document.getElementById('div-produto-'+id);
                div.parentNode.removeChild(div);
                quantidade_produto--;
                document.getElementById('qtd_produto').value = quantidade_produto;
            }
          </script>