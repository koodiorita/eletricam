	<!-- AddProd -->
    <div class="modal fade" id="RetiraSalario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Retira de Salario</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="php/retira_salario.php" method="POST"  >
                        <select name="funcionario_retirada" id="funcionario_retirada" class="form-control">
                            <?php 
                            $sql = "select * from funcionario WHERE status = 1";
                            $res = mysqli_query($conn,$sql);
                            while($row = mysqli_fetch_array($res)){ ?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>    
                            <?php }?>
                        </select><br>       
                        <input type="numer" step="0.01" name="valor_retirada" class="form-control" placeholder="Valor de Retirada" /><br>
                        <select class="form-control" name="tipo">
                            <option value="Pagamento do Mes">Pagamento do Mes</option>
                            <option value="Adiantamento">Adiantamento</option>
                            <option value="Custo Operacional">Custo Operacional</option>
                        </select><br>
                        <textarea name="descricao_retirada" id="descricao_retirada" class="form-control" placeholder="Descrição ..."></textarea><br>
                        <button class="btn btn-success" type="submit" style="float: right">Retirar</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
        
      