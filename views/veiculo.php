<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql = "select * from veiculo";
$res = mysqli_query($conn, $sql);




?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Veículo
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddVeic">Adicionar</button>

			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Modelo</th>
							<th>Marca</th>
							<th>Cor</th>
							<th>Ano</th>
							<th>Placa</th>
							<th>Renavam</th>
							<th>Vencimento IPVA</th>
							<th>KM</th>
							<th width="10%">Editar</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Modelo</th>
							<th>Marca</th>
							<th>Cor</th>
							<th>Ano</th>
							<th>Placa</th>
							<th>Renavam</th>
							<th>Vencimento IPVA</th>
							<th>KM</th>
							<th width="10%">Editar</th>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {

						?>
							<tr>
								<td><?= $row['modelo']; ?></td>
								<td><?= $row['marca']; ?></td>
								<td><?= $row['cor']; ?></td>
								<td><?= $row['ano']; ?></td>
								<td><?= $row['placa']; ?></td>
								<td><?= $row['renavam']; ?></td>
								<td><?= date('d/m/Y', strtotime($row['vencimento'])); ?></td>
								<td><?= number_format($row['km'], 0, '', '.'); ?></td>
								<td>
									<center><button class="btn btn-warning btn-circle" onclick="edit_veiculo(<?= $row['id']; ?>)"><i class="fas fa-edit"></i></button></center>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<div class="modal fade" id="EditVeic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Veículo</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_veiculo.php" method="POST">
					<input type="hidden" id="id_veic_edit" name="id_veic_edit">
					<div class="form-row">
						<div class="col">
							<input name="modelo_veic_edit" id="modelo_veic_edit" type="text" placeholder="Modelo" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="marca_veic_edit" id="marca_veic_edit" type="text" placeholder="Marca" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="cor_veic_edit" id="cor_veic_edit" type="text" placeholder="Cor" class="form-control" required /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input name="ano_veic_edit" id="ano_veic_edit" type="number" placeholder="Ano" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="placa_veic_edit" id="placa_veic_edit" type="text" placeholder="Placa" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="renavam_veic_edit" id="renavam_veic_edit" type="text" placeholder="Renavam" class="form-control" required /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<label style="color:grey;">Vencimento IPVA</label>
							<input name="vencimento_veic_edit" id="vencimento_veic_edit" type="date" class="form-control" required /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input name="km_veic_edit" id="km_veic_edit" type="text" placeholder="Quilometragem" class="form-control"><br>
						</div>
					</div>


					<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});


	function edit_veiculo(id) {
		$.get("php/get_veiculo.php?id_veiculo=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id_veic_edit").val(id);
			$("#modelo_veic_edit").val(json[0].modelo);
			$("#marca_veic_edit").val(json[1].marca);
			$("#cor_veic_edit").val(json[2].cor);
			$("#ano_veic_edit").val(json[3].ano);
			$("#placa_veic_edit").val(json[4].placa);
			$("#renavam_veic_edit").val(json[5].renavam);
			$("#vencimento_veic_edit").val(json[6].vencimento);
			$("#km_veic_edit").val(json[7].km);

			$('#EditVeic').modal('show');

		});

	}
</script>