<?php
session_start();

require_once("../conn/conexao.php");

if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}
function getNomeBanco($id){
	global $conn;
	if($id==0){
		$nome = "Nenhum banco";
	}else{
		$sql = "SELECT * FROM banco WHERE id = $id";
		$res = mysqli_query($conn, $sql);
		while ($row = mysqli_fetch_array($res)){
			$nome = $row['nome'];
		}
	}
	return $nome;
}
// Trazendo informações da tabela ordem_serv e contas_receber
$sql = " select
			cr.id as id,
			cr.id as id_ordem_serv,
			cr.responsavel as responsavel,
			cr.parcela as parcela,
			cr.valor_parcela as valor_parcela,
			cr.vencimento as vencimento,
			cr.tipo as tipo,
			cr.status as status,
			cr.banco as banco,
			os.id as id_os,
			os.data_comp as data_comp
		from
			ordem_serv as os 
			inner join contas_receber as cr on
			cr.id_ordem_serv = os.id
		";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM banco";
$resBanco = mysqli_query($conn, $sql);

?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Contas a Receber</h4>
				</div>

				<div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
				<div class="col-2">
					<select class="form-control" name="banco_pesquisa" id="banco_pesquisa">
						<option value="">Selecione o Banco</option>
						<?php while ($row = mysqli_fetch_array($resBanco)) { ?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['nome']; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar()">Buscar</button></div>
			</div>
		</div>
		<div class="card-body">
			<div class="table-responsive" id="div-table">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Responsável</th>
								<th>Data de Competência</th>
								<th>Data de Vencimento</th>
								<th>Parcela</th>
								<th>Valor da Parcela</th>
								<th>Tipo</th>
								<th>Banco</th>
								<th>Status</th>
								<th>Confirmação</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Responsável</th>
								<th>Data de Competência</th>
								<th>Data de Vencimento</th>
								<th>Parcela</th>
								<th>Valor da Parcela</th>
								<th>Tipo</th>
								<th>Banco</th>
								<th>Status</th>
								<th>Confirmação</th>
							</tr>
						</tfoot>
						<tbody>
							<?php

							while ($row = mysqli_fetch_array($res)) {
								$status = $row['status'];

								if ($status == 0) {
									$status = "Não recebido";
								}
								if ($status == 1) {
									$status = "Recebido";
								}
								if ($status == 2) {
									$status = "Cancelado";
								}

								if (is_null($row['banco'])) {
									$banco = 0;
								} else {
									$banco = $row['banco'];
								}
							?>
								<tr>
									<td><?= $row['responsavel']; ?></td>
									<td><?= date('d/m/Y', strtotime($row['data_comp'])); ?></td>
									<td><?= date('d/m', strtotime($row['vencimento'])); ?></td>
									<td><?= $row['parcela']; ?></td>
									<td><?= number_format($row['valor_parcela'], 2, ',', '.'); ?></td>
									<td><?= $row['tipo']; ?></td>
									<td><?= getNomeBanco($banco) ?></td>
									<td><?= $status; ?></td>
									<?php if ($row['status'] == 0) { ?>
										<td>
											<center>
												<a class="btn btn-danger btn-circle" href="php/reprova_conta.php?id=<?php echo $row['id']; ?>">
													<i class="fas fa-times"></i>
												</a>
												<a class="btn btn-success btn-circle" onclick="aprovarConta(<?= $row['id']; ?>)">
													<i class="fas fa-check"></i>
												</a>
											</center>
										</td>
									<?php } else {
										echo "<td>$status</td>";
									} ?>
								</tr>
							<?php } ?>
						</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
<div class="modal fade" id="AprovarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Receber Conta</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/aprova_conta.php" method="POST">
					<input id="id_conta" name="id_conta" type="hidden" />
					<div class="form-row">
						<div class="col">
							<label style="color: grey;">Selecione o banco</label>
							<select name="selectBanco" id="selectBanco" class="form-control">
								<?php
								$sql = "SELECT * FROM banco";
								$resultado = mysqli_query($conn, $sql);
								if (mysqli_num_rows($resultado) > 0) {
									while ($row = mysqli_fetch_assoc($resultado)) {
										$idBanco = $row['id'];
										$nomeBanco = $row['nome'];
										echo
											"<option value='" . $idBanco . "'>" . $nomeBanco . "</option>";
									}
								} else {
									echo "<option value=''>	Nenhum banco cadastrado </option> ";
								}
								?>
							</select><br>
						</div>
					</div>
					<button class="btn btn-success" type="submit" style="float: right">Confirmar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});

	function aprovarConta(id_conta) {
		$('#id_conta').val(id_conta);
		$('#AprovarConta').modal('show');

	}

	function buscar() {
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();
		var banco = $("#banco_pesquisa").val();

		if (data1.length > 0 || banco != "") {
			$.get("php/filtro_data.php?ini=" + data1 + "&fim=" + data2 + "&banco=" + banco, function(data) {
				$("#div-table").html(data);
			});
		} else {
			alert('Preencha pelo menos um dos campos.');
		}
	}
</script>