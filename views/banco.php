<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql = "select * from banco";
$res = mysqli_query($conn, $sql);




?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Banco
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddBanco">Adicionar</button>

			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Agencia</th>
							<th>Conta</th>
							<th>Endereço</th>
							<th>Cidade</th>
							<th>Valor</th>
							<th>Editar</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$total = 0;
						while ($row = mysqli_fetch_array($res)) {
							$total += $row['valor'];
						?>
							<tr>
								<td><?= $row['nome']; ?></td>
								<td><?= $row['agencia']; ?></td>
								<td><?= $row['conta']; ?></td>
								<td><?= $row['endereco']; ?></td>
								<td><?= $row['cidade']; ?></td>
								<td><?php echo "R$ " . number_format($row['valor'], 2, ',', '.'); ?></td>
								<td>
									<center><button class="btn btn-warning btn-circle" onclick="edit_banco(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i></button></center>
								</td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<th>Nome</th>
							<th>Agencia</th>
							<th>Conta</th>
							<th>Endereço</th>
							<th>Cidade</th>
							<th><?= "R$ " . number_format($total, 2, ',', '.'); ?></th>
							<th>Editar</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

</div>

<!-- EditaBanco -->
<div class="modal fade" id="EditBanco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Banco</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_banco.php" method="POST">
					<input type="hidden" id="id_banco_edit" name="id_banco_edit">
					<div class="form-row">
						<div class="col-12">
							<input id="nome_banco_edit" name="nome_banco_edit" type="text" placeholder="Nome do Banco" class="form-control" required /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col-4">
							<input id="agencia_edit" name="agencia_edit" type="text" placeholder="Ex: 0000-X" class="form-control" /><br>
						</div>
						<div class="col">
							<input id="conta_edit" name="conta_edit" type="text" placeholder="Ex: 00000000-0" class="form-control" /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input id="endereco_banco_edit" name="endereco_banco_edit" type="text" placeholder="Endereço completo" class="form-control" required /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">
							<input id="cidade_banco_edit" name="cidade_banco_edit" type="text" placeholder="Cidade" class="form-control" required /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">
							<input id="valor_banco_edit" name="valor_banco_edit" type="text" placeholder="Valor em Banco" class="form-control" required />
						</div>
					</div><br>

					<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});

	function edit_banco(id) {
		$.get("php/get_banco.php?id_banco=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id_banco_edit").val(id);
			$("#nome_banco_edit").val(json[0].nome);
			$("#agencia_edit").val(json[1].agencia);
			$("#conta_edit").val(json[2].conta);
			$("#endereco_banco_edit").val(json[3].endereco);
			$("#cidade_banco_edit").val(json[4].cidade);
			$("#valor_banco_edit").val(json[5].valor);
			$('#EditBanco').modal('show');
		});
	}
</script>