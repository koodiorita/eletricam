<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql = "select * from user";
$res = mysqli_query($conn, $sql);


?>

<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Usuário
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddUser">Adicionar</button>

			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Login</th>
							<th>Contato</th>
							<th width="10%">Editar</th>
							<?php if ($usuario_id == 1) { ?>
								<th width="10%">Permissões</th>
							<?php } ?>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Nome</th>
							<th>Login</th>
							<th>Contato</th>
							<th width="10%">Editar</th>
							<?php if ($usuario_id == 1) { ?>
								<th width="10%">Permissões</th>
							<?php } ?>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {

						?>
							<tr>
								<td><?php echo $row['nome']; ?></td>
								<td><?php echo $row['usuario']; ?></td>
								<td><?php echo $row['email']; ?></td>
								<td>
									<center>
										<?php if ($row['id'] != 1) { ?>
											<button class="btn btn-warning btn-circle" onclick="edit_usuario(<?php echo $row['id']; ?>)">
												<i class="fas fa-edit"></i>
											</button>
											<?php } else {
											if ($usuario_id == 1) { ?>
												<button class="btn btn-warning btn-circle" onclick="edit_usuario(<?php echo $row['id']; ?>)">
													<i class="fas fa-edit"></i>
												</button>
											<?php } else { ?>
												Usuário Master
										<?php }
										} ?>
									</center>
								</td>
								<?php if ($usuario_id == 1) { ?>
									<td>
										<center>
											<?php if ($row['id'] != 1) { ?>
												<button class="btn btn-warning btn-circle" onclick="edit_permissoes(<?php echo $row['id']; ?>)">
													<i class="fas fa-user-lock"></i>
												</button>
											<?php } else { ?>
												Usuário Master
											<?php } ?>
										</center>
									</td>
								<?php } ?>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<!-- AddUser -->
<div class="modal fade" id="EditUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Usuário</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_usuario.php" method="POST">
					<input type="hidden" id="id_user_edit" name="id_user_edit">
					<div class="form-row">
						<div class="col">
							<input name="nome_user_edit" id="nome_user_edit" type="text" placeholder="Nome de Usuário" class="form-control" required /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">
							<input name="usuario_user_edit" id="usuario_user_edit" type="text" placeholder="Login" class="form-control" required /><br>
						</div>
						<?php if ($usuario_id == 1) { ?>
							<div class="col">
								<input name="senha_user_edit" id="senha_user_edit" type="password" placeholder="Nova senha" class="form-control" /><br>
							</div>
						<?php } ?>
					</div>
					<div class="form-row">
						<div class="col">
							<input type="text" name="email_user_edit" id="email_user_edit" placeholder="Email para contato" class="form-control" required><br>
						</div>
					</div>

					<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="EditPermissoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Permissões</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_permissoes_usuario.php" method="POST">
					<div class="form-row">
						<div class="col">
							<div id="div-info-usuario">

							</div>
						</div>
					</div><br>
					<input type="hidden" id="id_user_permissoes_edit" name="id_user_permissoes_edit">
					<div class="form-row">
						<div class="col">
							<label style="color:grey;">Selecione as permissões</label>
							<select multiple name="permissoes_edit[]" id="permissoes_edit">
								<option value="1">Painel de Controle</option>
								<option value="2">Orçamentos</option>
								<option value="3">Ordens de Serviços</option>
								<option value="4">Usuários</option>
								<option value="5">Clientes</option>
								<option value="6">Fornecedores</option>
								<option value="7">Funcionários</option>
								<option value="8">Veículos</option>
								<option value="9">Serviços</option>
								<option value="10">Produtos</option>
								<option value="11">Bancos</option>
								<option value="12">Contas a Receber</option>
								<option value="13">Contas a Pagar</option>
							</select>
						</div>
					</div><br>

					<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});



	function edit_usuario(id) {
		$.get("php/get_usuario.php?id_usuario=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id_user_edit").val(id);
			$("#nome_user_edit").val(json[0].nome);
			$("#usuario_user_edit").val(json[1].usuario);
			$("#email_user_edit").val(json[3].email);


			$('#EditUser').modal('show');

		});

	}

	function edit_permissoes(id) {
		$("#id_user_permissoes_edit").val(id);

		$.get("php/get_info_usuario.php?id_usuario=" + id, function(data) {
			$("#div-info-usuario").html(data);
		});

		$('#EditPermissoes').modal('show');
	}

	$(function() {
		$('#permissoes_edit').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: true,
			create: false
		});
	});
</script>