<?php 
session_start();
include_once('../conn/conexao.php');

if(!empty($_SESSION['ZWxldHJpY2Ft'])){
  $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	exit(header('Location: login.php'));
}

$sql = "SELECT 
	month(c.data_cad) AS mes_order,
	case 
		when MONTH(c.data_cad) = 1 then 'Jan'
		when month(c.data_cad) = 2 then 'Fev'
		when month(c.data_cad) = 3 then 'Mar'
		when month(c.data_cad) = 4 then 'Abr'
		when month(c.data_cad) = 5 then 'Mai'
		when month(c.data_cad) = 6 then 'Jun'
		when month(c.data_cad) = 7 then 'Jul'
		when month(c.data_cad) = 8 then 'Ago'
		when month(c.data_cad) = 9 then 'Set'
		when month(c.data_cad) = 10 then 'Out'
		when month(c.data_cad) = 11 then 'Nov'
		when month(c.data_cad) = 12 then 'Dez'
	END AS mes,
	COUNT(c.data_cad) AS qtd
FROM 
	cliente AS c
WHERE 
	YEAR(data_cad) = YEAR(NOW()) and status = 1
GROUP BY mes
ORDER BY mes_order";
$res = mysqli_query($conn,$sql);


$mes = array();
$qtd = array();

$quantidade = 0;
while($row = mysqli_fetch_array($res)){
    $quantidade += $row['qtd'];
    array_push($mes,$row['mes']);
    array_push($qtd,$quantidade);
}

$mes = "['".implode("','", $mes)."']";
$qtd = "['".implode("','", $qtd)."']";


/*
$sql = "select * from caixa";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $valor = $row['valor'];
}
*/

// INFORMAR VALOR NOS BANCOS
$total=0;
$sqlBanco = "SELECT * FROM banco";
$resBanco = mysqli_query($conn,$sqlBanco);
while($row = mysqli_fetch_array($resBanco)){
  $total += $row['valor'];
}

// INFORMAR CONTAS RECEBER
$sql = "select * from contas_receber where status = 0 and month(vencimento) = month(now()) and year(vencimento) = year(now())";

$res = mysqli_query($conn,$sql);
$valor_receber = 0;
while($row = mysqli_fetch_array($res)){
    $valor_receber += $row['valor_parcela'];
}

$sql = "select * from contas_pagar where status = 0 and month(vencimento) = month(now()) and year(vencimento) = year(now())";

$res = mysqli_query($conn,$sql);
$valor_pagar = 0;
while($row = mysqli_fetch_array($res)){
    $valor_pagar += $row['valor'];
}

$sql = "SELECT COUNT(id) FROM ordem_serv WHERE status = 0";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $pendente = $row[0];
}


?>
<div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Dashboard</h1> <span><i onclick="ver()"  id="ver" class="fas fa-eye text-white" style="font-size: 18px"></i><input type="checkbox" id="ver-radio" style="display:none" ></span>
          
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- 
           - - - - -  
           VALOR CAIXA  
           - - - - -
         -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Bancos</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="caixa-show" style="display: none"><?= "R$ ".number_format($total, 2, ',', '.');?></div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="caixa-show2" >- - - -</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                  </div>
                </div>  
              </div>
              
              <a class="text-primary" onclick="page('historico_caixa')" style="margin-left: 20px;cursor: pointer" >Histórico</a>
            </div>
          </div>



         <!-- 
           - - - - -  
           VALOR A RECEBER  
           - - - - -
         -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">A Receber</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="receber-show" style="display: none"><?= "R$ ".number_format($valor_receber, 2, ',', '.');?></div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="receber-show2" >- - - -</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- 
           - - - - -  
           VALOR A PAGAR  
           - - - - -
         -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">A Pagar</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="pagar-show" style="display: none"><?php echo "R$ "
                    .number_format($valor_pagar, 2, ',', '.');?></div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="pagar-show2" >- - - -</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- 
           - - - - -  
           CONTRATOS PENDENTES  
           - - - - -
         -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">OS Abertas</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="orcamento-show" style="display: none"><?php echo $pendente; ?></div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800" id="orcamento-show2" >- - - -</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Content Row -->

        <div class="row">

          <!-- Area Chart -->
          <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Gráficos de Serviços</h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="chart-area">
                  <canvas id="myAreaChart">
                  </canvas>
                </div>
              </div>
            </div>
          </div>

          <!-- Pie Chart -->
          <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Meta Mensal</h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="chart-pie pt-4 pb-2">
                  <canvas id="myPieChart"></canvas>
                </div>
                <div class="mt-4 text-center small">
                  <span class="mr-2">
                    <i class="fas fa-circle text-primary"></i>&nbspAndamento
                  </span>
                
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Anotações</h1>
        </div>
        <!-- ANOTAÇÕES -->
        <div class="row">

          <!-- 
          - - - - - - - - - - - - - -
          WHILE DE ANOTAÇÕES
          - - - - - - - - - - - - - -
          -->
          <?php 
          $sql = "select a.id,a.assunto,a.mensagem,u.nome,a.data_cad,a.id_user from anotacao as a inner join user as u on a.id_user = u.id  where status = 0";
          $res = mysqli_query($conn,$sql);
          
          if(!empty($_SESSION['ZWxldHJpY2Ft'])){
            $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
          }else{
            $usuario_id = 0;
          }
          while($row = mysqli_fetch_array($res)){ ?>
            <div class="col-lg-3 mb-4">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Assunto: <?php echo $row['assunto'];?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Lembrete</div>
                      <a class="dropdown-item" href="php/anotacao_feita.php?id=<?php echo $row['id'];?>">Feito</a>
                      <?php if($row['id_user'] ==  $usuario_id ){ ?>
                      <a class="dropdown-item" style="cursor: pointer" onclick="editar_anotacao(<?php echo $row['id'];?>)">Editar</a>
                      <?php }?>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                      <?php echo $row['mensagem'];?>
                      <p style="margin-top: 10px;color: #a2a2a2;font-style: italic;font-size: 12px"><?php echo $row['nome'];?> <?php echo date('d/m/Y H:i', strtotime($row['data_cad']));?></p>
                </div>
              </div>
            </div>
          <?php }?>

          
          <!-- 
          - - - - - - - - - - - - - -
          FIM WHILE DE ANOTAÇÕES
          - - - - - - - - - - - - - -
          -->
          
        </div>
        <!-- FIM ANOTAÇÕES -->
             

        </div>
		   </div>
           

           	<!-- AddProd -->
    <div class="modal fade" id="EditAnotacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nova Anotação</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_anotacao.php" method="POST"  >
            <input type="hidden" class="form-control" name="id_anotacao_edit" id="id_anotacao_edit" placeholder="Assunto" ><br>
            <input type="text" class="form-control" name="assunto_edit" id="assunto_edit" placeholder="Assunto" ><br>
            <textarea name="mensagem_edit" id="mensagem_edit" class="form-control" height="200px" placeholder="Mensagem ......"></textarea><br>
						<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
      
<script>


function ver(){
    if ($('#ver-radio').is(':checked')) {
      $('#ver').removeClass('fas fa-eye-slash text-white');
      $('#ver').addClass('fas fa-eye text-white');
      $('#ver-radio').attr( 'checked', false );

      $('#caixa-show').css('display','none');
      $('#receber-show').css('display','none');
      $('#pagar-show').css('display','none');
      $('#orcamento-show').css('display','none');

      $('#caixa-show2').css('display','block');
      $('#receber-show2').css('display','block');
      $('#pagar-show2').css('display','block');
      $('#orcamento-show2').css('display','block');

    }else{
      $('#ver').removeClass('fas fa-eye text-white');
      $('#ver').addClass('fas fa-eye-slash text-white');
      $('#ver-radio').attr( 'checked', true );

      $('#caixa-show').css('display','block');
      $('#receber-show').css('display','block');
      $('#pagar-show').css('display','block');
      $('#orcamento-show').css('display','block');

      $('#caixa-show2').css('display','none');
      $('#receber-show2').css('display','none');
      $('#pagar-show2').css('display','none');
      $('#orcamento-show2').css('display','none');


    }
    
    
}

function editar_anotacao(id){
    $.get("php/get_anotacao.php?id="+id,function(data){
        var json = JSON.parse(data);
        $( "#id_anotacao_edit" ).val( id );
        $( "#assunto_edit" ).val( json[0].assunto );
        $( "#mensagem_edit" ).val( json[1].mensagem );
        $("#EditAnotacao").modal('show');
    });
}



// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: <?php echo $mes;?>,
    datasets: [{
      label: "1",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: <?php echo $qtd;?>,
    }]
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return value;
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return tooltipItem.yLabel;
        }
      }
    }
  }
});

</script>


<?php 

$meta = 0;
$sql = "SELECT 
	sum(valor_parcela) AS total
FROM 
	contas_receber 
WHERE 	
	MONTH(vencimento) = MONTH(NOW()) AND YEAR(vencimento) = YEAR(NOW())";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)){
    $total = $row[0];
}

$andamento = ($meta - $total) ;

if($andamento < 0){
  $labels = "'Andamento'";
  $valores = $total;
}else{
  $labels = "'Andamento','Faltando'";
  $valores = $total.",".$andamento;
}
?>
<script>
// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: [<?php echo $labels;?>],
    datasets: [{
      // [ANDAMENTO, (META - ANDAMTENTO]
      data: [<?php echo $valores;?>],
      backgroundColor: ['#4e73df'],
      hoverBackgroundColor: ['#2e59d9'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});

</script>