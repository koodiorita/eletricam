<?php
error_reporting(0);
include_once("../conn/conexao.php");
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo'); 

$orcamento_id = $_GET['id'];

$sql = "select * from orcamento as o inner join pagamento as p on o.pagamento_id = p.id  where o.id = $orcamento_id";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $id         = $row['id'];
    $cliente    = $row['cliente'];
    $inicio     = $row['previsao_inicio'];
    $execucao   = $row['previsao_execucao'];
    $status     = $row['status'];
    $data_cad   = $row['data_cad'];
    $pagamento  = $row['nome'];
}

if($status == 0){
    $status = "Aguardando retorno";
}
if($status == 1){
    $status = "Orçamento Aprovado";
}
if($status == 2){
    $status = "Orçamento Reprovado";
}

$endereco = "";
$cnpj = "";



$sql = "select * from cliente where razao_social like '%$cliente%'";
$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $cnpj           = $row['cnpj'];
    $razao_social   = $row['razao_social'];
    $responsavel    = $row['responsavel'];
    $endereco       = $row['endereco'];
    $cidade         = $row['cidade'];
    $bairro         = $row['bairro'];
    $numero         = $row['numero'];
}



?>



<!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Orçamento</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">
  <link href="img/icon.png" rel="shortcut icon">
 
 <style>
 .header{
    margin: 20px
 }
 label{
    display: block;
    margin-bottom: 0rem;
    font-size: 1.5rem;
 }
 .divider{
     position: relative;
     height: 1px;
     width: 100%;
     background: #101010;
     margin-top: 15px;
 }
 body{
     color: #000;
 }
 .assinatura{
    text-align: center;
    margin-top: 10%;
 }

 </style>
</head>
<body>
    <div class="header" >
        <div class="form-row">
            <div class="col">
                <img src="../img/eletricam.png" width="100%" />
            </div>
            <div class="col text-right">
                <h4><b>Orçamento Nº <span><?php echo $id;?></span></b></h4>
                <label>Eletricam Instalações Elétricas</label>
                <label>contato@eletricam.com.br</label>
                <label>(15) 99715-2014</label>
            </div>
        </div>
    </div>
    <br><br>
    <label class="divider"></label>
    <br><br>
    <div class="descricao">
   
        <div class="form-row">
            <div class="col">
                <label><b>Data Emissão:     </b><span><?php echo date('d',strtotime($data_cad))." de ".strftime('%B',strtotime($data_cad))." de ".date('Y',strtotime($data_cad));?></span></label><br>
                <label><b>Dados do Cliente  </b></label>
                <label><b>Nome:             </b><span><?php echo $cliente;?></span></label><br>
                <label><b>CNPJ:             </b><span><?php echo $cnpj;?></span></label>
                <label><b>Endereço:         </b><span><?php echo $endereco;?></span></label>
            </div>
            <div class="col">
                <label><b>Situação do Orçamento:    </b><span><?php echo $status;?></span></label><br>
                <label><b>Dados do Serviço          </b></label>
                <label><b>Inicio:                   </b><span><?php echo $inicio;?> dias</span></label><br>
                <label><b>Execução:                 </b><span><?php echo $execucao;?> dias</span></label>
                <label><b>Forma de Pagamento:       </b><span><?php echo $pagamento;?></span></label>
            </div>
        </div>
    </div>
    <br><br>
    <label class="divider"></label>
    <br><br>
    <div class="servicos_produtos">
    <div class="form-row">
            <div class="col-8">
                <label><b>Descrição</b></label>
            </div>
            <div class="col-2">
                <label><b>Qtd</b></label>
            </div>
            <div class="col-2">
                <label><b>Valor</b></label>
            </div>
        </div>
    <?php
        $sql = "select 
                    * 
                from 
                    orcamento as o 
                    inner join orcamento_serv as os on
                    o.id = os.orcamento_id
                    inner join servico as s on
                    os.servico_id = s.id 
                where 
                   o.id = $orcamento_id";
        $res = mysqli_query($conn, $sql);
        while($row = mysqli_fetch_array($res)){
            $valor_total_servico += $row['valor']*$row['qtd'];
    ?>
        
        <div class="form-row">
            <div class="col-8">
                <label><?php echo $row['nome'];?></label>
            </div>
            <div class="col-2">
                <label><?php echo $row['qtd'];?></label>
            </div>
            <div class="col-2">
                <label>R$ <?php echo number_format($row['valor']*$row['qtd'],2,'.','');?></label>
            </div>
        </div>
        <?php }?>
        <?php
         $sql = "select 
                    p.descricao,
                    op.qtd,
                    op.valor,
                    p.unidade 
                from 
                    orcamento as o 
                    inner join orcamento_prod as op on
                    o.id = op.orcamento_id
                    inner join produto as p on
                    op.produto_id = p.id 
                where 
                   o.id = $orcamento_id";
        $res = mysqli_query($conn, $sql);
        while($row = mysqli_fetch_array($res)){
            $valor_total_produto += $row['valor']*$row['qtd'];
    ?>
        
        <div class="form-row">
            <div class="col-8">
                <label><?php echo $row['descricao'];?></label>
            </div>
            <div class="col-2">
                <label><?php echo $row['qtd']." ".strtolower($row['unidade']);?></label>
            </div>
            <div class="col-2">
                <label>R$ <?php echo number_format($row['valor']*$row['qtd'],2,'.','');?></label>
            </div>
        </div>
        <?php }
        $total_geral = $valor_total_produto + $valor_total_servico;
        ?>
        <br>
        <div class="form-row">
            <div class="col-8">
                <label><b>Total Geral</b></label>
            </div>
            <div class="col-2">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label><b>R$ <?php echo number_format($total_geral,2,'.','');?></b></label>
            </div>
        </div>
    </div>
    <br>
    <label class="divider"></label>
    <br><br>
    <div class="assinatura">
        <label>___________________________________</label>
        <label><b>Fernando Aparecido Camargo</b></label>
        <label>Engenheiro responsável</label>
        <label>CREA: 5062237383</label>
    </div>

</body>
<script>window.print();</script>
<script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>
</html>