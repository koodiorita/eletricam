<?php
session_start();

require_once("../conn/conexao.php");

if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql = "select * from user where id = $usuario_id";

$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
	$nome 			= $row['nome'];
	$email 			= $row['email'];
	$avatar 			= $row['avatar'];
}

$sql = "SELECT valor FROM valor_hora";
$res_valor_hora = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res_valor_hora)){
	$valor_hora = $row[0];
}

?>
<div class="container-fluid">

	<img src="<?php echo $avatar; ?>" class="img-thumbnail" style="width: 150px; border-radius: 150px;margin-right: 20px" />
	<button class="btn btn-primary" data-toggle="modal" data-target="#ModalEditFoto">Alterar Foto</button><br><br>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<form action="php/editar_dados.php" method="POST" class="col-6">
					<label>Meus Dados</label>
					<input type="text" class="form-control col-10" name="nome" value="<?php echo $nome; ?>" placeholder="Nome da Empresa" /><br>
					<input type="text" class="form-control col-10" name="email" value="<?php echo $email; ?>" placeholder="Email da Empresa" /><br>

					<button type="submit" class="btn btn-success" style="float: left">Salvar</button>
				</form>
				<form action="php/redefinir_senha.php" method="POST" class="col-6">
					<label>Redefinir Senha</label>
					<input type="password" class="form-control col-10" name="senha1" placeholder="Digite a nova senha" /><br>
					<input type="password" class="form-control col-10" name="senha2" placeholder="Digite a senha novamente" /><br>

					<button type="submit" class="btn btn-success" style="float: left">Salvar</button>
				</form>
			</div>

		</div>
	</div>


</div>



<div class="modal fade" id="ModalEditFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Foto</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/editar_foto.php" method="POST" enctype="multipart/form-data">
					<div style="display: flex">
						<img src="<?php echo $avatar; ?>" class="img-thumbnail" style="width: 150px; border-radius: 150px;margin-right: 20px" />
						<input type="file" name="arquivo" class="form-control" style="align-self: center;" />
					</div><br>
					<center><button type="submit" class="btn btn-primary">Alterar Foto</button></center>
				</form>
			</div>
		</div>
	</div>
</div>