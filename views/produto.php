<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
	exit(header('Location: login.php'));
}


$sql = "select * from produto
		";
$res = mysqli_query($conn, $sql);




?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold text-primary">Produtos
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddProd">Adicionar</button>

			</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Descricao</th>
							<th width="10%">Preco</th>
							<th width="10%">Estoque</th>
							<th width="10%">Editar</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Descricao</th>
							<th width="10%">Preco</th>
							<th width="10%">Estoque</th>
							<th width="10%">Editar</th>
						</tr>
					</tfoot>
					<tbody>
						<?php

						while ($row = mysqli_fetch_array($res)) {

						?>
							<tr>
								<td><?php echo $row['descricao']; ?></td>
								<td><?php echo "R$ " . number_format($row['preco'], 2, ',', '.'); ?></td>
								<td><?php echo $row['estoque']; ?></td>
								<td>
									<center><button class="btn btn-warning btn-circle" onclick="edit_produto(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i></button></center>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<div class="modal fade" id="EditProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Produto</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_produto.php" method="POST">
					<input type="hidden" id="id_prod_edit" name="id_prod_edit">
					<div class="form-row">
						<div class="col">
							<input name="descricao_edit" id="descricao_edit" type="text" placeholder="Nome da Produto" class="form-control" required /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<input name="custo_edit" id="custo_edit" type="number" step="0.01" placeholder="Custo" class="form-control" /><br>
						</div>
						<div class="col">
							<input name="preco_edit" id="preco_edit" type="number" step="0.01" placeholder="Preço" class="form-control" /><br>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<select class="form-control" name="unidade_edit" id="unidade_edit">
								<option value="UN">UNIDADE</option>
								<option value="MT">METRO</option>
							</select>
						</div>
						<div class="col">
							<input name="estoque_edit" id="estoque_edit" type="number" step="0.01" placeholder="Preço" class="form-control" /><br>
						</div>
						<div class="col">
							<input name="minimo_edit" id="minimo_edit" type="number" step="0.01" placeholder="Custo" class="form-control" /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">
							<textarea name="obs_edit" id="obs_edit" type="text" placeholder="Observação" class="form-control"></textarea><br>
						</div>
					</div>
					<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});


	function edit_produto(id) {
		$.get("php/get_produtos.php?id_produto=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id_prod_edit").val(json[0].id);
			$("#descricao_edit").val(json[1].descricao);
			$("#preco_edit").val(json[2].preco);
			$("#custo_edit").val(json[3].custo);
			$("#estoque_edit").val(json[4].estoque);
			$("#minimo_edit").val(json[5].minimo);
			$("#unidade_edit").val(json[6].unidade);
			$("#obs_edit").val(json[7].obs);
			$('#EditProd').modal('show');
		});
	}
</script>