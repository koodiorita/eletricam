<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id			= $_POST['id_serv_edit'];
$nome			= $_POST['nome_serv_edit'];
$descricao		= $_POST['descricao_serv_edit'];


//Validação dos campos
if(empty($_POST['nome_serv_edit'])  ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#servico")); 
}else{
	//Salvar no BD
	$result_data = "update servico set nome = '$nome',descricao = '$descricao' where id = $id";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Serviço alterado com sucesso</div>";
		exit(header("Location: ../index.php#servico"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar serviço</div>";
		exit(header("Location: ../index.php#servico"));
	}
	
}


mysqli_close($conn);


?>