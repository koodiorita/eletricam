<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_for_edit'];
$nome			= $_POST['nome_for_edit'];
$cnpj			= $_POST['cnpj_for_edit'];
$cep			= $_POST['cep_for_edit'];
$endereco		= $_POST['endereco_for_edit'];
$numero			= $_POST['numero_for_edit'];
$bairro			= $_POST['bairro_for_edit'];
$cidade			= $_POST['cidade_for_edit'];
$telefone		= $_POST['telefone_for_edit'];
$telefone2		= $_POST['telefone2_for_edit'];


//Validação dos campos
if(empty($_POST['nome_for_edit']) || empty($_POST['endereco_for_edit']) || empty($_POST['bairro_for_edit'])  || empty($_POST['cidade_for_edit']) || empty($_POST['numero_for_edit']) || empty($_POST['cep_for_edit']) || empty($_POST['telefone_for_edit']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#fornecedor")); 
}else{
	//Salvar no BD
    $result_data = "update fornecedor set nome = '$nome', cnpj = '$cnpj' ,endereco = '$endereco',numero = '$numero',
    bairro = '$bairro',cidade = '$cidade',cep = '$cep',telefone = '$telefone', telefone2 = '$telefone2' where id = $id ";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Fornecedor alterado com sucesso</div>";
		exit(header("Location: ../index.php#fornecedor"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar fornecedor</div>";
		exit(header("Location: ../index.php#fornecedor"));
	}
	
}


mysqli_close($conn);


?>