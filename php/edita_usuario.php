<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_user_edit'];
$nome			= $_POST['nome_user_edit'];
$usuario		= $_POST['usuario_user_edit'];
if ($_POST['senha_user_edit']) {
	$senha			= $_POST['senha_user_edit'];
}
$email			= $_POST['email_user_edit'];


//Validação dos campos
if (empty($_POST['nome_user_edit']) || empty($_POST['usuario_user_edit']) || empty($_POST['email_user_edit'])) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#usuario"));
} else {
	if ($_POST['senha_user_edit']) {
		//Salvar no BD
		$result_data = "update user set nome = '$nome', usuario = '$usuario', senha = '$senha',email = '$email' where id = $id ";
		$resultado_data = mysqli_query($conn, $result_data);

		//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
		if ($resultado_data) {
			$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Usuário alterado com sucesso</div>";
			exit(header("Location: ../index.php#usuario"));
		} else {
			$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar usuário</div>";
			exit(header("Location: ../index.php#usuario"));
		}
	}else{
		//Salvar no BD
		$result_data = "update user set nome = '$nome', usuario = '$usuario',email = '$email' where id = $id ";
		$resultado_data = mysqli_query($conn, $result_data);

		//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
		if ($resultado_data) {
			$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Usuário alterado com sucesso</div>";
			exit(header("Location: ../index.php#usuario"));
		} else {
			$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar usuário</div>";
			exit(header("Location: ../index.php#usuario"));
		}
	}
}


mysqli_close($conn);
