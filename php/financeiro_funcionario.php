<?php
session_start();

require_once("../conn/conexao.php");

$id = $_GET['id'];
$sql = "SELECT 
            f.nome,
			c.vencimento,
			c.valor,
			c.descricao,
			c.status
		FROM 
			contas_pagar AS c
			INNER JOIN funcionario AS f ON
			c.id_fornecedor = f.id
		WHERE  
            f.id = $id AND
            c.tipo_responsavel = 'Funcionario'
        ORDER BY c.vencimento ASC
		";

$res = mysqli_query($conn, $sql);

?>

<div class="card-body">
    <div class="table-responsive">
        <table style="color:black;" class="table table-bordered" id="dataTableFinanceiro" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Vencimento</th>
                    <th>Responsavel</th>
                    <th>Valor</th>
                    <th>Descricao</th>
                    <th>Status</th>
                </tr>
            </thead>

            <tbody>
                <?php
                $valor_total = 0;
                while ($row = mysqli_fetch_array($res)) {
                    $valor = $row['valor'];
                    $valor_total += $valor;
                ?>
                    <tr>
                        <td><?php echo date('d/m/Y', strtotime($row['vencimento'])); ?></td>
                        <td><?php echo $row['nome']; ?></td>
                        <td><?php echo "R$ " . number_format($valor, 2, '.', ''); ?></td>
                        <td><?php echo $row['descricao']; ?></td>
                        <?php if ($row['status'] == 0) { ?>
                            <td>
                                <center>
                                    Ainda não foi pago
                                </center>
                            </td>
                        <?php } else { ?>
                            <td>
                                <center>Pago</center>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Vencimento</th>
                    <th>Responsavel</th>
                    <th>Valor</th>
                    <th>Descricao</th>
                    <th>Status</th>
                </tr>
            </tfoot>
        </table>

    </div>
</div>