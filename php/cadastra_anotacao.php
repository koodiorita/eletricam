<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

if(!empty($_SESSION['ZWxldHJpY2Ft'])){
    $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
  }else{
    exit(header('Location: login.php'));
  }

$assunto  = $_POST['assunto'];
$mensagem = $_POST['mensagem'];

if(empty($_POST['assunto']) || empty($_POST['mensagem'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#dashboard")); 
}else{
    $sql = "insert into anotacao (assunto,mensagem,id_user) values('$assunto','$mensagem',$usuario_id)";
    $res = mysqli_query($conn,$sql);

    if($res){
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Anotação cadastrada com sucesso</div>";
		exit(header("Location: ../index.php#dashboard"));	
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar anotação</div>";
		exit(header("Location: ../index.php#dashboard"));	
    }
}