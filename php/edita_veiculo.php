<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_veic_edit'];
$modelo			= $_POST['modelo_veic_edit'];
$marca			= $_POST['marca_veic_edit'];
$cor			= $_POST['cor_veic_edit'];
$ano			= $_POST['ano_veic_edit'];
$placa			= $_POST['placa_veic_edit'];
$renavam		= $_POST['renavam_veic_edit'];
$vencimento		= $_POST['vencimento_veic_edit'];
$km				= $_POST['km_veic_edit'];


//Validação dos campos
if(empty($_POST['modelo_veic_edit']) || empty($_POST['marca_veic_edit']) || empty($_POST['cor_veic_edit'])  || empty($_POST['ano_veic_edit']) || 
	empty($_POST['placa_veic_edit']) || empty($_POST['renavam_veic_edit']) || empty($_POST['vencimento_veic_edit']) || empty($_POST['km_veic_edit']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#veiculo")); 
}else{
	//Salvar no BD
    $result_data = "update veiculo set modelo = '$modelo', marca = '$marca' ,cor = '$cor',ano = '$ano', placa = '$placa', renavam = '$renavam',
    vencimento = '$vencimento', km = '$km' where id = $id ";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Veículo alterado com sucesso</div>";
		exit(header("Location: ../index.php#veiculo"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar veículo</div>";
		exit(header("Location: ../index.php#veiculo"));
	}
	
}


mysqli_close($conn);


?>