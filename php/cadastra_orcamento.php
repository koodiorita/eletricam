<?php
error_reporting(0);
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getIdOrcamento(){
	global $conn;

	$sql = "select * from orcamento order by id desc limit 1";

	$res = mysqli_query($conn,$sql);

	$id = 0;
	while($row = mysqli_fetch_array($res)){
		$id = $row['id'];
	}

	return $id;
}

function getValorProduto($id){
	global $conn;

	$sql = "select preco from produto where id = $id limit 1";
	$res = mysqli_query($conn,$sql);

	while($row = mysqli_fetch_array($res)){
		$valor = $row['preco'];
	}


	return $valor;

}



//Receber os dados do formulário

$servico_orc = array();
$produto_orc = array();
$qtd_serv_orc = array();
$qtd_prod_orc = array();
$valor_orc	  = array();


$nome				= $_POST['cliente_orc'];
$qtd_servico    	= $_POST['qtd_servico'];
array_push($servico_orc,$_POST['servico_orc_0']);
array_push($qtd_serv_orc,$_POST['qtd_serv_0']);
array_push($valor_orc,$_POST['valor_orc_0']);
$qtd_produto		= $_POST['qtd_produto'];
array_push($produto_orc,$_POST['produto_orc_0']);
array_push($qtd_prod_orc,$_POST['qtd_prod_0']);
$previsao_inicio	= $_POST['previsao_inicio_orc'];
$previsao_execucao	= $_POST['previsao_execucao_orc'];
$pagamento			= $_POST['pagamento_orc'];


//Validação dos campos
if(empty($_POST['cliente_orc']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#orcamento")); 
}else{
	//Salvar no BD
 	$result_data = "INSERT INTO orcamento(cliente,previsao_inicio,previsao_execucao,pagamento_id)
	 				 value('$nome',$previsao_inicio,$previsao_execucao,$pagamento)";
	$resultado_data = mysqli_query($conn, $result_data);

	$id_orcamento = getIdOrcamento();


	if($qtd_servico > 1){
		for($i = 1;$i < $qtd_servico; $i++){
			array_push($servico_orc,$_POST['servico_orc_'.$i]);
			array_push($qtd_serv_orc,$_POST['qtd_serv_orc_'.$i]);
			array_push($valor_orc,$_POST['valor_orc_'.$i]);
		}
	}


	if($qtd_produto > 1){
		for($i = 1;$i < $qtd_produto; $i++){
			array_push($produto_orc,$_POST['produto_orc_'.$i]);
			array_push($qtd_prod_orc,$_POST['qtd_prod_orc_'.$i]);
		}
	}


	
	foreach($servico_orc as $indice => $valor){
		$servico_id 	= $servico_orc[$indice];
		$qtd	 		= $qtd_serv_orc[$indice];
		$valor 			= $valor_orc[$indice];

		if(!empty($servico_id) || !empty($qtd) || !empty($valor)){
			$sql = "insert into orcamento_serv(servico_id,orcamento_id,qtd,valor) values($servico_id,$id_orcamento,$qtd,$valor)";
			mysqli_query($conn,$sql);
		}
		

	}


	foreach($produto_orc as $indice => $valor){
		$produto_id 	= $produto_orc[$indice];
		$qtd	 		= $qtd_prod_orc[$indice];
		$valor 			= getValorProduto($produto_id);


		if(!empty($produto_id) || !empty($qtd) || !empty($valor)){
			$sql = "insert into orcamento_prod(produto_id,orcamento_id,qtd,valor) values($produto_id,$id_orcamento,$qtd,$valor)";
			mysqli_query($conn,$sql);
		}
	}

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Orçamento cadastrado com sucesso</div>";
		echo"<script type='text/javascript' language='Javascript'>window.open('../views/print_orcamento.php?id=$id_orcamento');</script>";	
		//header("Location: ../index.php#orcamento");
		echo"<script type='text/javascript' language='Javascript'>window.location.href='../index.php#orcamento'</script>";
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar orçamento</div>";
		exit(header("Location: ../index.php#orcamento"));
	}
	
}


mysqli_close($conn);


?>