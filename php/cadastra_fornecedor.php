<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$nome			= $_POST['nome_for'];
$cnpj			= $_POST['cnpj_for'];
$cep			= $_POST['cep_for'];
$endereco		= $_POST['endereco_for'];
$numero			= $_POST['numero_for'];
$bairro			= $_POST['bairro_for'];
$cidade			= $_POST['cidade_for'];
$telefone		= $_POST['telefone_for'];
$telefone2		= $_POST['telefone2_for'];


//Validação dos campos
if(empty($_POST['nome_for']) || empty($_POST['endereco_for']) || empty($_POST['bairro_for'])  || empty($_POST['cidade_for']) || empty($_POST['numero_for']) || empty($_POST['cep_for']) || empty($_POST['telefone_for']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#fornecedor")); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO fornecedor(nome,cnpj,endereco,numero,bairro,cidade,cep,telefone,telefone2) 
    value('$nome','$cnpj','$endereco','$numero','$bairro','$cidade','$cep','$telefone','$telefone2')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Fornecedor cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#fornecedor"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar fornecedor</div>";
		exit(header("Location: ../index.php#fornecedor"));
	}
	
}


mysqli_close($conn);


?>