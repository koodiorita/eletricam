<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_prod_edit'];
$descricao		= $_POST['descricao_edit'];
$custo			= $_POST['custo_edit'];
$preco			= $_POST['preco_edit'];
$estoque		= $_POST['estoque_edit'];
$minimo			= $_POST['minimo_edit'];
$unidade 		= $_POST['unidade_edit'];
$obs 			= $_POST['obs_edit'];



//Validação dos campos
if(empty($_POST['id_prod_edit']) || empty($_POST['descricao_edit']) || empty($_POST['custo_edit'])  || empty($_POST['preco_edit']) || 
	empty($_POST['estoque_edit']) || empty($_POST['minimo_edit']) || empty($_POST['unidade_edit']) || empty($_POST['obs_edit']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#produto")); 
}else{
	//Salvar no BD
    $result_data = "update produto set descricao = '$descricao', preco = $preco, custo = $custo, estoque = $estoque, minimo = $minimo,
    unidade = '$unidade', obs = '$obs' where id = $id ";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Produto alterado com sucesso</div>";
		exit(header("Location: ../index.php#produto"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar produto</div>";
		exit(header("Location: ../index.php#produto"));
	}
	
}


mysqli_close($conn);


?>