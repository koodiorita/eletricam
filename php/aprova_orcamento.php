<?php
session_start();
include_once("../conn/conexao.php");

$id_orcamento = $_POST['id_orcamento'];
$dataComp = $_POST['dataCompetencia'];//mudar para tabela OS
$dataVencimento = $_POST['dataVencimento']; 
$sql = "update orcamento set status = 1 where id = $id_orcamento";
$res = mysqli_query($conn,$sql);

$sql = "select
			sum(os.valor*os.qtd) as valor_serv,
			p.qtd_vezes as parcelas,
			p.id as tipo,
			o.cliente as responsavel
		from
			orcamento as o
			left join orcamento_serv as os on
			o.id = os.orcamento_id  
			inner join pagamento as p on
			o.pagamento_id = p.id
		where 
			o.id = $id_orcamento
		";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
	$valorServ = $row['valor_serv'];
	$tipo = $row['tipo'];
	$parcelas = $row['parcelas'];
	$responsavel = $row['responsavel'];
}

$sql = "select
			sum(op.valor*op.qtd) as valor_prod
		from
			orcamento as o
			left join orcamento_prod as op on
			o.id = op.orcamento_id  
		where 
			o.id = $id_orcamento
		";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
	$valorProd = $row['valor_prod'];
}

$valor_total = $valorServ + $valorProd;
$valor_parcela = number_format($valor_total / $parcelas, 2, '.', '');
echo $sql = "INSERT INTO ordem_serv(id_orcamento, data_comp, data_vencimento, status, tipo,responsavel,valor_total) 
	VALUES($id_orcamento, '$dataComp', '$dataVencimento',0,'$tipo','$responsavel',$valor_total)";
$res = mysqli_query($conn,$sql);


$sql = "SELECT id FROM ordem_serv WHERE id_orcamento=$id_orcamento limit 1";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)){
	$id_ordem_serv = $row['id'];
}


if ($res) {
	for ($i=0; $i < $parcelas; $i++) { 
		$parcela = ($i+1)."/".$parcelas;
		$sql = "INSERT INTO contas_receber(id_ordem_serv,responsavel,parcela,valor_parcela,vencimento,tipo)
		VALUES($id_ordem_serv,'$responsavel','$parcela',$valor_parcela,'$dataVencimento','$tipo')";
		mysqli_query($conn,$sql);
		$dataVencimento = date('Y-m-d', strtotime('+30 days', strtotime($dataVencimento)));
	}
}

if($res){
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Aprovado com sucesso</div>";
	exit(header("Location: ../index.php#orcamento")); 
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Erro ao tentar aprovar</div>";
	exit(header("Location: ../index.php#orcamento")); 
}
?>