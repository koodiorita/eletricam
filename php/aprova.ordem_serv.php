<?php

session_start();
include_once("../conn/conexao.php");
function getKmVeiculo($id_veiculo){
	global $conn;
	$sql = "SELECT km FROM veiculo WHERE id=$id_veiculo";
	$res = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($res);
	return $row[0];
}

$id_ordem = $_POST['id_ordem'];
$obs = $_POST['observacao'];
$id_funcionario = $_POST['selectFuncionario'];
$id_veiculo = $_POST['selectVeiculo'];
$hora_ini = $_POST['horaInicio'];
$hora_fim = $_POST['horaFim'];
$km_ini = $_POST['kmInicio'];
$km_fim = $_POST['kmFim'];
$contratante = $_POST['contratante'];

if ($hora_ini<$hora_fim) {
	if ($km_ini<$km_fim) {
		$sql = "update ordem_serv set status = 1,obs = '$obs',data_fechamento = now(), id_funcionario=$id_funcionario, id_veiculo=$id_veiculo,hora_ini='$hora_ini',hora_fim='$hora_fim',km_ini=$km_ini,km_fim=$km_fim,contratante='$contratante' where id = $id_ordem";
		$res = mysqli_query($conn,$sql);

		if($res){
			$km_rodado = $km_fim-$km_ini;
			$km_atual = getKmVeiculo($id_veiculo);
			$km_final = $km_atual+$km_rodado;
			$sql = "UPDATE veiculo set km=$km_final WHERE id=$id_veiculo";
			mysqli_query($conn,$sql);
		    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Aprovado com sucesso</div>";
			exit(header("Location: ../index.php#ordem_servico")); 
		}else{
		    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Erro ao tentar aprovar</div>";
			exit(header("Location: ../index.php#ordem_servico")); 
		}
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>KM inicial não pode ser maior nem igual o KM final</div>";
		exit(header("Location: ../index.php#ordem_servico")); 
	}
}else{
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Hora inicial não pode ser maior nem igual a hora final</div>";
	exit(header("Location: ../index.php#ordem_servico")); 
}



?>

