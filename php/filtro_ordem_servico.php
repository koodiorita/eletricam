<?php
session_start();
require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	exit(header('Location: login.php'));
}

$aprovado = $_GET['aprovado'] != null ? $_GET['aprovado'] : 0;

$sql = "SELECT id, id_orcamento, data_comp, data_vencimento, data_fechamento, obs, status,tipo,responsavel,valor_total
FROM ordem_serv WHERE status=$aprovado";

$res = mysqli_query($conn,$sql);

?>
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Responsável</th>
        <th>Data de Competência</th>
        <th>Data de Vencimento</th>
        <th>Valor Total</th>
        <th>Tipo</th>
        <th>Observação</th>
        <th>Data de Fechamento</th>
        <th>Status</th>
        <th>Confirmação</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Responsável</th>
        <th>Data de Competência</th>
        <th>Data de Vencimento</th>
        <th>Valor Total</th>
        <th>Tipo</th>
        <th>Observação</th>
        <th>Data de Fechamento</th>
        <th>Status</th>
        <th>Confirmação</th>
    </tr>
    </tfoot>
    <tbody>
        <?php
            
        while($row = mysqli_fetch_array($res)) { 
            $status = $row['status'];

            if($status == 0){
                $status = "Não executado";
            }
            if($status == 1){
                $status = "Executado";
            }
            if($status == 2){
                $status = "Cancelado";
            }


            if (is_null($row['obs'])) {
                $obs = "Sem observação";
            }else{
                $obs = $row['obs'];
            }
            if (is_null($row['data_fechamento'])) {
                $data_fechamento = "Em aberto";
            }else{
                $data_fechamento = date('d/m/y',strtotime($row['data_fechamento']));
            }
            ?>
            <tr>
                <td><?= $row['responsavel'];?></td>
                <td><?= date('d/m/Y',strtotime($row['data_comp']));?></td>
                <td><?= date('d/m',strtotime($row['data_vencimento']));?></td>
                <td><?= $row['valor_total'];?></td>
                <td><?= $row['tipo'];?></td>
                <td><?= $obs;?></td>
                <td><?= $data_fechamento;?></td>
                <td><?= $status;?></td>
                <?php if($row['status'] == 1){ ?>
                <td>
                    <center>
                         <a class="btn btn-primary btn-circle" target="_BLANK" href="views/print_OS.php?id=<?php echo $row['id'];?>" >
                            <i class="fas fa-print" ></i>
                        </a>
                    </center>
                </td>
                <?php }else if($row['status'] == 0){ ?>
                <td>
                <center>
                <a class="btn btn-danger btn-circle" href="php/reprova.ordem_serv.php?id=<?php echo $row['id'];?>" >
                    <i class="fas fa-times" ></i>
                </a>
                <a class="btn btn-success btn-circle"  onclick="executadoOrdem(<?= $row['id']; ?>)" >
                    <i class="fas fa-check" ></i>
                </a>
                </center>
                </td>
                <?php }else if($row['status'] == 2){?>
                <td>Cancelado</td>
                <?php } ?>
            </tr>
        <?php }?>	
    </tbody>
</table>
