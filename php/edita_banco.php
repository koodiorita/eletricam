<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_banco_edit'];
$nome			= $_POST['nome_banco_edit'];
$agencia		= $_POST['agencia_edit'];
$conta			= $_POST['conta_edit'];
$endereco		= $_POST['endereco_banco_edit'];
$cidade			= $_POST['cidade_banco_edit'];
$valor			= $_POST['valor_banco_edit'];



//Validação dos campos

if(empty($_POST['nome_banco_edit']) || empty($_POST['agencia_edit']) || empty($_POST['conta_edit']) || empty($_POST['endereco_banco_edit']) || 
	empty($_POST['cidade_banco_edit']) || empty($_POST['valor_banco_edit'])){

	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#banco")); 

}else{
	//Salvar no BD

    $result_data = "update banco set nome = '$nome', agencia = '$agencia', conta = '$conta', endereco = '$endereco',cidade = '$cidade', valor='$valor' where id = $id ";
    
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Banco alterado com sucesso</div>";
		exit(header("Location: ../index.php#banco"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar banco</div>";
		exit(header("Location: ../index.php#banco"));
	}
	
}


mysqli_close($conn);


?>