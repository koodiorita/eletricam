<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$valor_hora = $_POST['valor_hora'];

//Salvar no BD
$sql = "UPDATE valor_hora SET valor = $valor_hora";
$res = mysqli_query($conn, $sql);

//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
if ($res) {
	$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Valor da hora alterado com sucesso</div>";
	exit(header("Location: ../index.php#configuracoes"));
} else {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar valor da hora</div>";
	exit(header("Location: ../index.php#configuracoes"));
}

mysqli_close($conn);
