<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$nome			= $_POST['nome_func'];
$cpf			= $_POST['cpf_func'];
$cep			= $_POST['cep_func'];
$endereco		= $_POST['endereco_func'];
$numero			= $_POST['numero_func'];
$bairro			= $_POST['bairro_func'];
$cidade			= $_POST['cidade_func'];
$telefone		= $_POST['telefone_func'];
$radio_cnh		= $_POST['radio_cnh'];

if ($radio_cnh == 1) {
	$cnh			= $_POST['cnh_func'];
	$validade		= $_POST['validade_func'];
}


//Validação dos campos
if (empty($_POST['nome_func']) || empty($_POST['endereco_func']) || empty($_POST['bairro_func'])  || empty($_POST['cidade_func']) || empty($_POST['numero_func']) || empty($_POST['cep_func']) || empty($_POST['telefone_func'])) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#funcionario"));
} else {
	if ($radio_cnh == 1) {
		//Salvar no BD
		$result_data = "INSERT INTO funcionario(nome,cpf,endereco,numero,bairro,cidade,cep,telefone,cnh,validade) 
    value('$nome','$cpf','$endereco','$numero','$bairro','$cidade','$cep','$telefone','$cnh','$validade')";
		$resultado_data = mysqli_query($conn, $result_data);
	} else {
		$result_data = "INSERT INTO funcionario(nome,cpf,endereco,numero,bairro,cidade,cep,telefone) 
    value('$nome','$cpf','$endereco','$numero','$bairro','$cidade','$cep','$telefone')";
		$resultado_data = mysqli_query($conn, $result_data);
	}

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if (mysqli_insert_id($conn)) {
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Funcionario cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#funcionario"));
	} else {
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar funcionario</div>";
		exit(header("Location: ../index.php#funcionario"));
	}
}


mysqli_close($conn);
