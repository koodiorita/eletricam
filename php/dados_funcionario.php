<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$id = $_GET['id'];

$sql = "select * from funcionario where id = $id and status = 1";

$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $nome = $row['nome'];
    $cpf = $row['cpf'];
    $endereco = $row['endereco'];
    $numero = $row['numero'];
    $bairro = $row['bairro'];
    $cidade = $row['cidade'];
    $cep = $row['cep'];
    $telefone = $row['telefone'];
    $cnh = $row['cnh'];
    $validade = $row['validade'];
}

?>
<br>
<form action="php/edita_funcionario.php" method="POST">
    <input type="hidden" id="id_func_edit" name="id_func_edit" value="<?= $id ?>">
    <div class="form-row">
        <div class="col">
            <input name="nome_func_edit" id="nome_func_edit" value="<?= $nome ?>" type="text" placeholder="Nome da Funcionario" class="form-control" required /><br>
        </div>
        <div class="col">
            <input name="cpf_func_edit" id="cpf_func_edit" type="text" placeholder="CPF" class="form-control" value="<?= $cpf ?>" /><br>
        </div>
    </div>

        <div class="col">
            <label style="color:grey;">CNH: </label>
            <div class="col">
                <input name="radio_cnh_edit" id="radio_cnh_1" onchange="cnh(this)" value="1" type="radio" checked> Sim
                <input name="radio_cnh_edit" id="radio_cnh_0" onchange="cnh(this)" value="0" type="radio"> Não <br>
            </div>
        </div>
        <div id="entrada_radio_nao" class="hide">
            <br>
            <div class="col">
                <label style="color:grey;">Sem CNH</label>
            </div>
        </div>
        <div id="entrada_radio_sim" class="show">
            <br>
            <div class="form-row">
                <div class="col">
                    <input name="cnh_func_edit" type="text" placeholder="CNH" class="form-control" value="<?= $cnh ?>"/><br>
                </div>
                <div class="col">
                    <input name="validade_func_edit" id="validade_func" type="date" class="form-control" value="<?= $validade ?>"/><br>
                </div>
            </div>
        </div>

    <div class="form-row">
        <div class="col-4">
            <input id="cep_func_edit" name="cep_func_edit" type="text" placeholder="CEP" class="form-control" required value="<?= $cep ?>" /><br>
        </div>
        <div class="col">
            <input value="Buscar Cep" id="buscaCepFunc_edit" type="button" class="btn btn-primary" /><br>
        </div>
    </div>
    <div class="form-row">
        <div class="col-8">
            <input id="endereco_func_edit" name="endereco_func_edit" type="text" placeholder="Endereço completo" class="form-control" required value="<?= $endereco ?>" /><br>
        </div>
        <div class="col-4">
            <input id="numero_func_edit" name="numero_func_edit" type="text" placeholder="Numero" class="form-control" required value="<?= $numero ?>" /><br>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <input id="bairro_func_edit" name="bairro_func_edit" type="text" placeholder="Bairro" class="form-control" required value="<?= $bairro ?>" /><br>
        </div>
        <div class="col">
            <input id="cidade_func_edit" name="cidade_func_edit" type="text" placeholder="Cidade" class="form-control" required value="<?= $cidade ?>" /><br>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <input name="telefone_func_edit" id="telefone_func_edit" type="text" placeholder="Telefone" class="form-control" value="<?= $telefone ?>" required />
        </div>
    </div><br>


    <button class="btn btn-success" type="submit" style="float: right">Alterar</button>
    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
</form>

<script>
    $(document).ready(function() {
        $("#buscaCepFor_edit").click(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_func_edit").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(cep)) {

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_func_edit").val(dados.logradouro);
                            $("#endereco_func_edit").css("background", "#eee");
                            $("#bairro_func_edit").val(dados.bairro);
                            $("#bairro_func_edit").css("background", "#eee");
                            $("#cidade_func_edit").val(dados.localidade);
                            $("#cidade_func_edit").css("background", "#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
        });
    });
    // $.get("php/get_funcionario.php?id_funcionario=" + id, function(data) {
    // 	var json = JSON.parse(data);
    // 	$("#id_func_edit").val(id);
    // 	$("#nome_func_edit").val(json[0].nome);
    // 	$("#endereco_func_edit").val(json[1].endereco);
    // 	$("#numero_func_edit").val(json[2].numero);
    // 	$("#bairro_func_edit").val(json[3].bairro);
    // 	$("#cidade_func_edit").val(json[4].cidade);
    // 	$("#cep_func_edit").val(json[5].cep);
    // 	$("#telefone_func_edit").val(json[6].telefone);
    // 	$("#cpf_func_edit").val(json[7].cpf);
    // 	$("#cnh_func_edit").val(json[8].cnh);
    // 	$("#validade_func_edit").val(json[9].validade);

    // 	$('#EditFunc').modal('show');

    // });
    function cnh(obj) {
        if (obj.value == 0) {
            $('#entrada_radio_nao').addClass("show");
            $('#entrada_radio_nao').removeClass("hide");

            $('#entrada_radio_sim').removeClass("show");
            $('#entrada_radio_sim').addClass("hide");

        } else {
            $('#entrada_radio_nao').removeClass("show");
            $('#entrada_radio_nao').addClass("hide");

            $('#entrada_radio_sim').removeClass("hide");
            $('#entrada_radio_sim').addClass("show");

        }
    }
</script>