<?php
session_start();

require_once("../conn/conexao.php");
function getNomeBanco($id){
	global $conn;
	if($id==0){
		$nome = "Nenhum banco";
	}else{
		$sql = "SELECT * FROM banco WHERE id = $id";
		$res = mysqli_query($conn, $sql);
		while ($row = mysqli_fetch_array($res)){
			$nome = $row['nome'];
		}
	}
	return $nome;
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$banco = $_GET['banco'];

$where = "";
if (!empty($data1)) {
    $where = "where cr.vencimento between '$data1' AND '$data2'";
    if (!empty($banco)) {
        $where .= " AND cr.banco = '$banco'";
    }
} else {
    if (!empty($banco)) {
        $where = "where cr.banco = '$banco'";
    }
}
//2020-11-01 2020-11-30
$sql = "select
            cr.id as id,
            cr.id as id_ordem_serv,
            cr.responsavel as responsavel,
            cr.parcela as parcela,
            cr.valor_parcela as valor_parcela,
            cr.vencimento as vencimento,
            cr.tipo as tipo,
            cr.status as status,
            cr.banco as banco,
            os.id as id_os,
            os.data_comp as data_comp
        from
            ordem_serv as os 
            inner join contas_receber as cr on
                cr.id_ordem_serv = os.id
        $where";
$res = mysqli_query($conn, $sql);

?>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Responsável</th>
            <th>Data de Competência</th>
            <th>Data de Vencimento</th>
            <th>Parcela</th>
            <th>Valor da Parcela</th>
            <th>Tipo</th>
            <th>Banco</th>
            <th>Status</th>
            <th>Confirmação</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Responsável</th>
            <th>Data de Competência</th>
            <th>Data de Vencimento</th>
            <th>Parcela</th>
            <th>Valor da Parcela</th>
            <th>Tipo</th>
            <th>Banco</th>
            <th>Status</th>
            <th>Confirmação</th>
        </tr>
    </tfoot>
    <tbody>
        <?php

        while ($row = mysqli_fetch_array($res)) {
            $status = $row['status'];

            if ($status == 0) {
                $status = "Não recebido";
            }
            if ($status == 1) {
                $status = "Recebido";
            }
            if ($status == 2) {
                $status = "Cancelado";
            }

            if (is_null($row['banco'])) {
                $banco = 0;
            } else {
                $banco = $row['banco'];
            }
        ?>
            <tr>
                <td><?= $row['responsavel']; ?></td>
                <td><?= date('d/m/Y', strtotime($row['data_comp'])); ?></td>
                <td><?= date('d/m', strtotime($row['vencimento'])); ?></td>
                <td><?= $row['parcela']; ?></td>
                <td><?= number_format($row['valor_parcela'], 2, ',', '.'); ?></td>
                <td><?= $row['tipo']; ?></td>
                <td><?= getNomeBanco($banco) ?></td>
                <td><?= $status; ?></td>
                <?php if ($row['status'] == 0) { ?>
                    <td>
                        <center>
                            <a class="btn btn-danger btn-circle" href="php/reprova_conta.php?id=<?php echo $row['id']; ?>">
                                <i class="fas fa-times"></i>
                            </a>
                            <a class="btn btn-success btn-circle" onclick="aprovarConta(<?= $row['id']; ?>)">
                                <i class="fas fa-check"></i>
                            </a>
                        </center>
                    </td>
                <?php } else {
                    echo "<td>$status</td>";
                } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('#tableFiltro').DataTable({});
    });
</script>