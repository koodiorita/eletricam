<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_produto = $_GET['id_produto'];

$sql = "select * from produto where id = $id_produto";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('id' => $row['id']));
	array_push($data,array('descricao' => $row['descricao']));
	array_push($data,array('preco' => $row['preco']));
	array_push($data,array('custo' => $row['custo']));
	array_push($data,array('estoque' => $row['estoque']));
	array_push($data,array('minimo' => $row['minimo']));
	array_push($data,array('unidade' => $row['unidade']));
	array_push($data,array('obs' => $row['obs']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>