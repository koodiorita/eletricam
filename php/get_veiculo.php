<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_veiculo = $_GET['id_veiculo'];

$sql = "select * from veiculo where id = $id_veiculo";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('modelo' => $row['modelo']));
	array_push($data,array('marca' => $row['marca']));
	array_push($data,array('cor' => $row['cor']));
	array_push($data,array('ano' => $row['ano']));
	array_push($data,array('placa' => $row['placa']));
	array_push($data,array('renavam' => $row['renavam']));
	array_push($data,array('vencimento' => $row['vencimento']));
	array_push($data,array('km' => $row['km']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>