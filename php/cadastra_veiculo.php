<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$modelo			= $_POST['modelo_veic'];
$marca 			= $_POST['marca_veic'];
$cor 			= $_POST['cor_veic'];
$ano			= $_POST['ano_veic'];
$placa			= $_POST['placa_veic'];
$renavam		= $_POST['renavam_veic'];
$ipva			= $_POST['vencimento_veic'];
$km				= $_POST['km_veic'];


//Validação dos campos
if(empty($_POST['modelo_veic']) || empty($_POST['marca_veic']) || empty($_POST['cor_veic'])  || empty($_POST['ano_veic']) || empty($_POST['placa_veic']) || empty($_POST['renavam_veic']) || empty($_POST['vencimento_veic']) || empty($_POST['km_veic']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#veiculo")); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO veiculo(id,modelo,marca,cor,ano,placa,renavam,vencimento,km) 
    VALUES(NULL,'$modelo','$marca','$cor','$ano','$placa','$renavam','$ipva','$km')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
	    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Veículo cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#veiculo"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar veículo</div>";
		exit(header("Location: ../index.php#veiculo"));
	}
	
}


mysqli_close($conn);


?>