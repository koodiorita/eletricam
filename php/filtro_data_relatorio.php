<?php
session_start();

require_once("../conn/conexao.php");

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$status = $_GET['status'];

$where = "";
if ($data1 != NULL) {
	$where = "WHERE data_comp BETWEEN '$data1' AND '$data2'";
	if ($status != NULL) {
		$where .= " AND status = $status";
	}
} else {
	if ($status != NULL) {
		$where = "WHERE status = $status";
	}
}

$sql = "SELECT * FROM ordem_serv $where";
$res = mysqli_query($conn, $sql);
?>


<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>Contratante</th>
			<th>Data de competência</th>
			<th>Valor total</th>
			<th>Observação</th>
			<th>Status</th>
			<th width="10%">Visualizar</th>
		</tr>
	</thead>
	<tbody>
		<?php
		while ($row = mysqli_fetch_array($res)) {
			if ($row['status'] == 0) {
				$status = "Não executado";
			} elseif ($row['status'] == 1) {
				$status = "Executado";
			} elseif ($row['status'] == 2) {
				$status = "Cancelado";
			}
			if (is_null($row['obs'])) {
				$obs = "Sem observação";
			} else {
				$obs = $row['obs'];
			}
		?>
			<tr>
				<td><?php echo $row['responsavel']; ?></td>
				<td><?php echo date('d/m/Y', strtotime($row['data_comp'])); ?></td>
				<td><?php echo "R$ " . number_format($row['valor_total'], 2, ',', '.'); ?></td>
				<td><?php echo $obs; ?></td>
				<td><?= $status; ?></td>
				<td>
					<center>
						<a target="_BLANK" class="btn btn-primary btn-circle" href="views/visualiza_OS.php?id=<?php echo $row['id']; ?>">
							<i class="fas fa-eye"></i>
						</a>
					</center>
				</td>
			</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Contratante</th>
			<th>Data de competência</th>
			<th>Valor total</th>
			<th>Observação</th>
			<th>Status</th>
			<th width="10%">Visualizar</th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() {
		$('#dataTable').DataTable({});
	});
</script>