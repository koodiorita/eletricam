<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$descricao		= $_POST['descricao'];
$preco			= $_POST['preco'];
$custo			= $_POST['custo'];
$estoque		= $_POST['estoque'];
$unidade		= $_POST['unidade'];
$minimo			= $_POST['minimo'];
$obs			= $_POST['obs'];


//Validação dos campos
if(empty($_POST['descricao']) || empty($_POST['preco']) || empty($_POST['unidade'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#funcionario")); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO produto(descricao,preco,custo,unidade,estoque,minimo,obs) 
    value('$descricao',$preco,$custo,'$unidade',$estoque,$minimo,'$obs')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
	    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Produto cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#produto"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar Produto</div>";
		exit(header("Location: ../index.php#produto"));
	}
	
}


mysqli_close($conn);


?>