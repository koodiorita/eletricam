<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

if (isset($_FILES["avatar"])) {

	//Testando extensão da imagem
	$ext = strtolower(end(explode('.', $_FILES['avatar']['name'])));
	$_UP['extensoes'] = array('jpg', 'png', 'jpeg');
	if (array_search($ext, $_UP['extensoes']) === false) {
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Por favor, envie arquivos com as seguintes extensões: jpg, png ou jpeg</div>";
		exit(header("Location: ../index.php#usuario")); 
	}else{
		$NameImg = $_FILES['avatar']['name'];
		$dir = '../img/';
		move_uploaded_file($_FILES['avatar']['tmp_name'], $dir.$NameImg);
	}
}

$nome = $_POST["nome"];
$usuario = $_POST["usuario"];
$senha = $_POST["senha"];
$email = $_POST["email"];
$imagem = "img/".$NameImg;

$permissoes = $_POST['permissoes'];

//Verificação se USERNAME já está sendo utilizado
$sql = "SELECT id FROM user WHERE usuario='$usuario'";
$res = mysqli_query($conn,$sql);
$contID = 0;
while ($row = mysqli_fetch_array($res)) {
	if (is_null($row['id'])) {
		break;
	}else{
		$contID += 1;
	}
}

//Validação dos campos
if(empty($_POST["nome"]) || empty($_POST["usuario"]) || empty($_POST["senha"]) || empty($_POST["email"]) || empty($_FILES['avatar']) || $contID>0){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Não foi possível cadastrar, campos incorretos ou login já cadastrado.</div>";
	exit(header("Location: ../index.php#usuario")); 
}else{
	//Salvar no BD
	$result_data = "INSERT INTO user (id,nome,usuario,senha,email,avatar) VALUES(NULL,'$nome','$usuario','$senha','$email','$imagem')";
	$resultado_data = mysqli_query($conn, $result_data);
	
	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
	    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Usuário cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#usuario"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar usuário</div>";
		exit(header("Location: ../index.php#usuario"));
	}
	
}


// PERMISSÕES DE TELAS
$sql = "SELECT id FROM user WHERE usuario='$usuario'";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
	$id_user = $row['id'];
}
echo $id_user;

for ($i=0; $i < count($permissoes); $i++) { 
	$sql = "INSERT INTO user_permission(id_user,permission) VALUES($id_user,$permissoes[$i])";
	mysqli_query($conn,$sql);
}


mysqli_close($conn);


?>