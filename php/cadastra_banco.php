<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$nome			= $_POST['nome_banco'];
$agencia		= $_POST['agencia'];
$conta			= $_POST['conta'];
$endereco		= $_POST['endereco_banco'];
$cidade			= $_POST['cidade_banco'];
$valor			= $_POST['valor_banco'];



//Validação dos campos
if(empty($_POST['nome_banco']) || empty($_POST['agencia']) || empty($_POST['conta']) || empty($_POST['endereco_banco']) ||empty($_POST['cidade_banco']) || empty($_POST['valor_banco']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#banco"));
}else{
	//Salvar no BD
	$result_data = "INSERT INTO banco(id,nome,agencia,conta,endereco,cidade,valor) 
	VALUES(NULL,'$nome','$agencia','$conta','$endereco','$cidade','$valor')";

	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
	    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Banco cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#banco"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar Banco</div>";
		exit(header("Location: ../index.php#banco"));
	}
	
}


mysqli_close($conn);


?>