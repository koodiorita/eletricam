<?php
session_start();

require_once("../conn/conexao.php");

function getResponsavel($id,$tipo){
	global $conn;

	if($tipo=="Fornecedor"){
		$sql = "SELECT nome FROM fornecedor WHERE id=$id";
	}else if($tipo=="Funcionario"){
		$sql = "SELECT nome FROM funcionario WHERE id=$id";
	}
	$res = mysqli_query($conn,$sql);
	while ($row = mysqli_fetch_array($res)) {
		$responsavel = $row[0];
	}
	return $responsavel;
}

if (!empty($_SESSION['ZWxldHJpY2Ft'])) {
  $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
} else {
  exit(header('Location: login.php'));
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$banco = $_GET['banco'];

$where = "";
if (!empty($data1)) {
  $where = "where c.vencimento between '$data1' AND '$data2'";
  if (!empty($banco)) {
    $where .= " AND c.banco = '$banco'";
  }
} else {
  if (!empty($banco)) {
    $where = "where c.banco = '$banco'";
  }
}

$sql = "SELECT 
          c.id,
          c.id_fornecedor,
          c.tipo_responsavel,
          c.valor,
          c.vencimento,
          c.dataCompetencia,
          c.descricao,
          c.status
        FROM 
          `contas_pagar` as c
        $where";
$res = mysqli_query($conn, $sql);

?>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Vencimento</th>
      <th>Data de Competência</th>
      <th>Valor</th>
      <th>Responsável</th>
      <th>Tipo de Responsável</th>
      <th>Descricao</th>
      <th width="10%">Pagar</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $total = 0;
    while ($row = mysqli_fetch_array($res)) {
      $total += $row['valor'];
      if(is_null($row['dataCompetencia'])){
        $data_competencia = "Sem data";
      }else{
        $data_competencia = date('d/m/Y', strtotime($row['dataCompetencia']));
      }
    ?>
      <tr>
        <td><?php echo date('d/m/Y', strtotime($row['vencimento'])); ?></td>
        <td><?php echo $data_competencia ?></td>
        <td><?php echo "R$ " . number_format($row['valor'], 2, '.', ''); ?></td>
        <td><?php echo getResponsavel($row['id_fornecedor'],$row['tipo_responsavel']);; ?></td>
        <td><?php echo $row['tipo_responsavel'] ?></td>
        <td><?php echo $row['descricao']; ?></td>
        <?php if ($row['status'] == 0) { ?>
          <td>
            <center><button class="btn btn-danger btn-circle" onclick="pagar(<?php echo $row['id']; ?>)"><i class="fas fa-download"></i></button></center>
          </td>
        <?php } else { ?>
          <td>Pago</td>
        <?php } ?>
      </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <th>Vencimento</th>
      <th>Data de Competência</th>
      <th><?php echo "R$ " . number_format($total, 2, '.', ''); ?></th>
      <th>Responsável</th>
      <th>Tipo de Responsável</th>
      <th>Descricao</th>
      <th width="10%">Pagar</th>
    </tr>
  </tfoot>
</table>