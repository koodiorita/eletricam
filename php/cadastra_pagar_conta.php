<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");
//VER O RADIO PARA SABER QUAL CAMINHO ESCOLHER
$radio = $_POST['radio_for_func'];

//1 PARA FORNECEDOR
if ($radio == 1) {

	//Receber os dados do formulário
	$id_fornecedor       = $_POST['fornecedor_pagar'];
	$valor              = $_POST['valor_pagar'];
	$vezes              = $_POST['vezes_pagar'];
	$vencimento         = $_POST['vencimento_pagar'];
	$descricao          = $_POST['descricao'];
	$dataCompetencia	= $_POST['dataCompetencia'];

	//Validação dos campos
	if (
		empty($_POST['fornecedor_pagar']) || empty($_POST['valor_pagar']) || empty($_POST['vencimento_pagar']) || empty($_POST['vezes_pagar'])
		|| empty($_POST['dataCompetencia'])
	) {
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
		exit(header("Location: ../index.php#contas_pagar"));
	} else {
		if($vezes>1){
			$valor /= $vezes;
		}
		for ($x = 0; $x < $vezes; $x++) {
			$result_data = "insert into contas_pagar (valor,id_fornecedor,vencimento,dataCompetencia,descricao,status,tipo_responsavel)
        values ($valor,$id_fornecedor,'$vencimento','$dataCompetencia','$descricao',0,'Fornecedor')";
			$res = mysqli_query($conn, $result_data);

			$vencimento = date('Y-m-d', strtotime('+30 days', strtotime($vencimento)));
		}

		//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
		if ($res) {
			$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta de fornecedor cadastrada com sucesso</div>";
			exit(header("Location: ../index.php#contas_pagar"));
		} else {
			$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar conta de fornecedor</div>";
			exit(header("Location: ../index.php#contas_pagar"));
		}
	}
	//0 PARA FUNCIONARIO
} else if ($radio == 0) {
	//Receber os dados do formulário
	$id_funcionario       = $_POST['funcionario_pagar'];
	$valor              = $_POST['valor_pagar_func'];
	$vezes              = $_POST['vencimento_pagar_func'];
	$descricao         = $_POST['descricao'];

	if (
		empty($_POST['funcionario_pagar']) || empty($_POST['valor_pagar_func'])
		|| empty($_POST['vencimento_pagar_func']) || empty($_POST['descricao'])
	) {
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
		exit(header("Location: ../index.php#contas_pagar"));
	} else {

		$result_data = "insert into contas_pagar (valor,id_fornecedor,vencimento,descricao,status,tipo_responsavel)
		values ($valor,$id_funcionario,'$vencimento','$descricao',0,'Funcionario')";
		$res = mysqli_query($conn, $result_data);

		if($res){
			$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta de funcionário cadastrada com sucesso</div>";
			exit(header("Location: ../index.php#contas_pagar"));
		}else{
			$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar conta de funcionário</div>";
			exit(header("Location: ../index.php#contas_pagar"));
		}
	}
}

mysqli_close($conn);
