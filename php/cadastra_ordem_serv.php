<?php
session_start();

include_once("../conn/conexao.php");

$responsavel = $_POST['responsavel_os'];
$pagamento = $_POST['pagamento_os'];
$valor = $_POST['valor_os'];
$data_vencimento = $_POST['data_vencimento'];
$data_competencia = $_POST['data_competencia'];

//INSERT DA TABELA OS
$sql = "INSERT INTO ordem_serv(data_comp,data_vencimento,tipo,responsavel,valor_total)
                VALUES('$data_competencia','$data_vencimento',$pagamento,'$responsavel',$valor)";
$res = mysqli_query($conn, $sql);

$sql = "SELECT LAST_INSERT_ID() FROM ordem_serv";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $id_os = $row[0];
}

if ($res) {
    //SELECT PAGAMENTO PARA SABER QUANTIDADE DE VEZES A CONTA SERÁ DIVIDIDA
    $sql = "SELECT * FROM pagamento WHERE id = $pagamento";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $tipo = $row['tipo'];
        $qtd_vezes = $row['qtd_vezes'];
    }

    $valor /= $qtd_vezes;

    for ($i = 0; $i < $qtd_vezes; $i++) {
        //INSERT DA TABELA CONTAS_RECEBER
        $parcela = $i + 1 . "/" . $qtd_vezes;
        $sql = "INSERT INTO contas_receber(id_ordem_serv,responsavel,parcela,valor_parcela,vencimento,tipo)
                    VALUES($id_os,'$responsavel','$parcela',$valor,'$data_vencimento','$tipo')";
        $res = mysqli_query($conn, $sql);


        $data_vencimento = date('Y-m-d', strtotime('+30 days', strtotime($data_vencimento)));
    }
    if ($res) {
        $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Ordem de serviço cadastrada com sucesso.</div>";
        exit(header("Location: ../index.php#ordem_servico"));
    } else {
        $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar ordem de serviço.</div>";
        exit(header("Location: ../index.php#ordem_servico"));
    }
} else {
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar ordem de serviço.</div>";
    exit(header("Location: ../index.php#ordem_servico"));
}
