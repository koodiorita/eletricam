<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getCliente(){
	global $conn;

	$sql = "select id from cliente order by id desc limit 1";
	$res = mysqli_query($conn,$sql);

	while($row = mysqli_fetch_array($res)){
		$id = $row['id'];
	}

	return $id;
}

//Receber os dados do formulário
$cnpj			= $_POST['cnpj'];
$razao			= $_POST['razao'];
$ie 			= $_POST['ie'] == NULL ? 'NULL' : $_POST['ie'];
$email 			= $_POST['email'] == NULL ? 'NULL' : $_POST['email'];
$responsavel	= $_POST['responsavel'] == NULL ? 'NULL' : $_POST['responsavel'];
$endereco		= $_POST['endereco'];
$numero 		= $_POST['numero'];
$bairro 		= $_POST['bairro'];
$cidade 		= $_POST['cidade'];
$cep    		= $_POST['cep'];
$telefone		= $_POST['telefone'];
$telefone2		= $_POST['telefone2'] == NULL ? 'NULL' : $_POST['telefone2'];
$whatsapp		= $_POST['whatsapp'] == NULL ? 'NULL' : $_POST['whatsapp'];
$dominio		= $_POST['dominio'] == NULL ? 'NULL' : $_POST['dominio'];
$observacao		= $_POST['observacao'] == NULL ? 'NULL' : $_POST['observacao'];


//Validação dos campos
if(empty($_POST['cnpj']) || empty($_POST['razao']) || empty($_POST['endereco']) || empty($_POST['bairro'])  || empty($_POST['cidade']) || empty($_POST['numero']) || empty($_POST['cep']) || empty($_POST['telefone']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#cliente")); 
}else{
	
	//Salvar no BD
	$result_data = "INSERT INTO cliente(cnpj,razao_social,ie,email,responsavel,endereco,numero,bairro,cidade,cep,telefone,telefone2,
	whatsapp,observacao,dominio) value('$cnpj','$razao','$ie','$email','$responsavel','$endereco','$numero','$bairro','$cidade','$cep','$telefone','$telefone2','$whatsapp','$observacao','$dominio')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Cliente cadastrado com sucesso</div>";
		exit(header("Location: ../index.php#cliente"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar cliente</div>";
		exit(header("Location: ../index.php#cliente"));
	}
	
}


mysqli_close($conn);


?>