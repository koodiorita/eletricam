<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	exit(header('Location: login.php'));
}

//Receber os dados do formulário
$senha		= $_POST['senha1'];
$senha2		= $_POST['senha2'];

if(empty($_POST['senha1']) || empty($_POST['senha2'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os dois campos</div>";
	exit(header("Location: ../index.php#minha_conta")); 
}else{
    if($senha === $senha2){
        $sql = "update user set senha = '$senha' where id = $usuario_id";
        $res = mysqli_query($conn,$sql);
        if($res){
            $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Senha Atualizada</div>";
            exit(header("Location: logout.php"));
        }else{
            $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao alterar senha</div>";
            exit(header("Location: ../index.php#minha_conta"));
        }
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Senhas não coincidem</div>";
        exit(header("Location: ../index.php#minha_conta"));
    }

}