<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_cli_edit'];
$cnpj			= $_POST['cnpj_cli_edit'];
$ie    			= $_POST['ie_cli_edit'];
$razao			= $_POST['razao_cli_edit'];
$email			= $_POST['email_cli_edit'];
$resposavel 	= $_POST['responsavel_cli_edit'];
$cep			= $_POST['cep_cli_edit'];
$endereco		= $_POST['endereco_cli_edit'];
$numero			= $_POST['numero_cli_edit'];
$bairro			= $_POST['bairro_cli_edit'];
$cidade			= $_POST['cidade_cli_edit'];
$telefone		= $_POST['telefone_cli_edit'];
$telefone2		= $_POST['telefone2_cli_edit'];
$whatsapp		= $_POST['whatsapp_cli_edit'];
$dominio		= $_POST['dominio_cli_edit'];
$observacao		= $_POST['observacao_cli_edit'];


//Validação dos campos
if(empty($_POST['id_cli_edit']) || empty($_POST['cnpj_cli_edit']) || empty($_POST['razao_cli_edit'])  || empty($_POST['responsavel_cli_edit'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	exit(header("Location: ../index.php#cliente")); 
}else{
	//Salvar no BD
  	$result_data = "update cliente set razao_social = '$razao', cnpj = '$cnpj' , ie = '$ie' , email = '$email', 
        responsavel = '$resposavel', endereco = '$endereco',numero = '$numero',
        bairro = '$bairro',cidade = '$cidade',cep = '$cep',telefone = '$telefone', telefone2 = '$telefone2', whatsapp = '$whatsapp' , 
        dominio = '$dominio', observacao = '$observacao' where id = $id ";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Cliente alterado com sucesso</div>";
		exit(header("Location: ../index.php#cliente"));		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar cliente</div>";
		exit(header("Location: ../index.php#cliente"));
	}
	
}


mysqli_close($conn);
