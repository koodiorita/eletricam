<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");



if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	exit(header('Location: login.php'));
}


//Receber os dados do formulário
$arquivo		= $_FILES['arquivo'];

$_UP['extensoes'] = array('jpg', 'png', 'jpeg');

$extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
if (array_search($extensao, $_UP['extensoes']) === false) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Por favor, envie arquivos com as seguintes extensões: jpg, png ou jpeg</div>";
	exit(header("Location: ../index.php#minha_conta")); 
}

//Validação dos campos
if(empty($_FILES['arquivo'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Selecione uma imagem</div>";
	exit(header("Location: ../index.php#minha_conta")); 
}else{
		$nome_final = "img/".$_FILES['arquivo']['name'];
		if (move_uploaded_file($_FILES['arquivo']['tmp_name'],  "../".$nome_final)) {
			$insertImagem = "update user set avatar = '$nome_final' where id = $usuario_id";
			$resInsertImagem = mysqli_query($conn,$insertImagem);
			
			if($resInsertImagem){
				$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Logo atualizado com sucesso</div>";
				exit(header("Location: ../index.php#minha_conta"));
			}else{
				$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Upload de Imagem com sucesso / Erro no update da imagem</div>";
				exit(header("Location: ../index.php#minha_conta"));
			}
			
		}else{
			$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao atualiar imagem</div>";
			exit(header("Location: ../index.php#minha_conta"));
		}
		
	
	
}


mysqli_close($conn);


?>